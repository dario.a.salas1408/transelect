﻿using System;
using System.Collections.Generic;
using System.Text;
using SAPbouiCOM.Framework;
using TranSelect.Forms;

namespace TranSelect
{
    class Menu
    {
        public void AddMenuItems()
        {
            SAPbouiCOM.Menus oMenus = null;
            SAPbouiCOM.MenuItem oMenuItem = null;

            oMenus = Application.SBO_Application.Menus;

            SAPbouiCOM.MenuCreationParams oCreationPackage = null;
            oCreationPackage = ((SAPbouiCOM.MenuCreationParams)(Application.SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)));
            oMenuItem = Application.SBO_Application.Menus.Item("43520"); // moudles'

            if (!oMenuItem.SubMenus.Exists("TranSelect"))
            {
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
                oCreationPackage.UniqueID = "TranSelect";
                oCreationPackage.String = "Ajuste en Pagos";
                oCreationPackage.Enabled = true;
                oCreationPackage.Position = -1;
                oCreationPackage.Image = AppDomain.CurrentDomain.BaseDirectory + @"Icon\US-dollar.png";
                oMenus = oMenuItem.SubMenus;
                oMenus.AddEx(oCreationPackage);
            }

            if (!oMenuItem.SubMenus.Exists("TranSelectSettings"))
            {
                oMenuItem = Application.SBO_Application.Menus.Item("TranSelect");
                oMenus = oMenuItem.SubMenus;

                // Create s sub menu
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "TranSelectSettings";
                oCreationPackage.String = "Configuracion General";
                //oCreationPackage.Enabled = true;
                //oCreationPackage.Position = 1;
                oMenus.AddEx(oCreationPackage);
            }
        }

        public void SBO_Application_MenuEvent(ref SAPbouiCOM.MenuEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;

            try
            {
                if (pVal.BeforeAction && pVal.MenuUID == "TranSelectSettings")
                {
                    Settings activeForm = new Settings();
                    activeForm.Show();
                }
            }
            catch (Exception ex)
            {
                Application.SBO_Application.MessageBox(ex.ToString(), 1, "Ok", "", "");
            }
        }

    }
}
