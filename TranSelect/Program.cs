﻿using System;
using System.Collections.Generic;
using SAPbouiCOM.Framework;
using TranSelect.Forms;
using TranSelect.SAPForms;

namespace TranSelect
{
    class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                Application oApp = null;
                if (args.Length < 1)
                {
                    oApp = new Application();
                }
                else
                {
                    oApp = new Application(args[0]);
                }

                SAPbouiCOM.SboGuiApi SboGuiApi;
                SboGuiApi = new SAPbouiCOM.SboGuiApi();

                Globals.SBOCompany = (SAPbobsCOM.Company)SAPbouiCOM.Framework.Application.SBO_Application.Company.GetDICompany();
                Globals.SBOApplication = SAPbouiCOM.Framework.Application.SBO_Application;
                Globals.ChooseDbQueryourceFile();

                DbStructure.Ini();

                Globals.SetVariables();

                Menu MyMenu = new Menu();
                MyMenu.AddMenuItems();

                FilterEvents();

                oApp.RegisterMenuEventHandler(MyMenu.SBO_Application_MenuEvent);
                Application.SBO_Application.AppEvent += new SAPbouiCOM._IApplicationEvents_AppEventEventHandler(SBO_Application_AppEvent);
                //Application.SBO_Application.MenuEvent += new SAPbouiCOM._IApplicationEvents_AppEventEventHandler(MenuEvent);
                Globals.SBOApplication.MenuEvent += MenuEvent;
                Application.SBO_Application.ItemEvent += SBO_Application_ItemEvent;

                Globals.SBOApplication.StatusBar.SetText("Addon Ajustes en Pagos iniciado correctamente!", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);

                oApp.Run();
            }
            catch (Exception ex)
            {
                Utils.LogError("Program.Main.Line" + Utils.GetLineException(ex) + ".Ex:", ex);
            }
        }

        static void SBO_Application_AppEvent(SAPbouiCOM.BoAppEventTypes EventType)
        {
            switch (EventType)
            {
                case SAPbouiCOM.BoAppEventTypes.aet_ShutDown:
                    //Exit Add-On
                    System.Windows.Forms.Application.Exit();
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_CompanyChanged:
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_FontChanged:
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_LanguageChanged:
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_ServerTerminition:
                    break;
                default:
                    break;
            }
        }

        private static void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent itemEvent, out bool BubbleEvent)
        {
            BubbleEvent = true;
            if (!itemEvent.BeforeAction)
            {
                switch (itemEvent.FormTypeEx)
                {
                    case "426":
                        Pagos_efectuados.Event(FormUID, ref itemEvent, out BubbleEvent);
                        break;
                    case "170":
                        //Pagos_recibidos.Event(FormUID, ref itemEvent, out BubbleEvent);
                        fhIncPayment.Event(FormUID, ref itemEvent, out BubbleEvent);
                        break;
                    case "TranSelect.Forms.Settings":
                        Settings.Event(FormUID, ref itemEvent, out BubbleEvent);
                        break;
                    default:
                        break;
                }
            }
            else
            {
                switch (itemEvent.FormTypeEx)
                {
                    case "170":
                        switch (itemEvent.EventType)
                        {
                            case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED:
                                fhIncPayment.Event(FormUID, ref itemEvent, out BubbleEvent);
                                break;
                        }
                        break;
                    case "426":
                        switch (itemEvent.EventType)
                        {
                            case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED:
                                Pagos_efectuados.Event(FormUID, ref itemEvent, out BubbleEvent);
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        public static void MenuEvent(ref SAPbouiCOM.MenuEvent menuEvent, out bool bubbleEvent)
        {
            if (menuEvent.MenuUID == "1284")
            {
                if ((Globals.SBOApplication.Forms.ActiveForm.Type) == 170)
                {
                    fhIncPayment.MenuCancel(menuEvent.BeforeAction);
                }
            }



            bubbleEvent = true;

        }

        private static void FilterEvents()
        {
            SAPbouiCOM.EventFilters filters = new SAPbouiCOM.EventFilters();
            SAPbouiCOM.EventFilter filter;
            try
            {

                filter = filters.Add(SAPbouiCOM.BoEventTypes.et_MENU_CLICK);
                filter.AddEx("65211"); //Production Order
                filter.AddEx("TranSelect.Forms.Settings");

                filter = filters.Add(SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED);
                //filter.AddEx("0");
                //filter.AddEx("139"); //Sales orders
                //filter.AddEx("1474000001"); //InventoryCounting
                //filter.AddEx("142"); //Purchase order
                //filter.AddEx("150");
                //filter.AddEx("1470000152"); // Pick Ticket 
                //filter.AddEx("65211"); //Production Order
                //filter.AddEx("65213"); //Issue For Production
                //filter.AddEx("1470000002");
                //filter.AddEx("60091"); //AR Reserve Invoice
                //filter.AddEx("60092"); //AP Reserve Invoice
                //filter.AddEx("1250000940");
                //filter.AddEx("62");
                //filter.AddEx("138");
                //filter.AddEx("85");
                //filter.AddEx("143");
                filter.AddEx("426");
                filter.AddEx("170");
                filter.AddEx("TranSelect.Forms.Settings");


                filter = filters.Add(SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD);
                //filter.AddEx("150"); //Item Master
                //filter.AddEx("65213"); //Issue For Production
                //filter.AddEx("134"); //BP
                //filter.AddEx("16642");
                //filter.AddEx("85");
                //filter.AddEx("65214");
                //filter.AddEx("140");
                //filter.AddEx("65211"); //Production Order
                //filter.AddEx("139"); //Sales Order
                //filter.AddEx("60091"); //AR Reserve Invoice
                //filter.AddEx("143"); //Good Receipt
                //filter.AddEx("426");
                //filter.AddEx("170");
                filter.AddEx("TranSelect.Forms.Settings");


                //filter = filters.Add(SAPbouiCOM.BoEventTypes.et_COMBO_SELECT);
                //filter.AddEx("150");
                //filter.AddEx("TranSelect.Forms.Settings");


                //filter = filters.Add(SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD);
                //filter.AddEx("62"); //Warehouse
                //filter.AddEx("134"); //Business Partner
                //filter.AddEx("139");
                //filter.AddEx("1474000001"); //InventoryCounting
                //filter.AddEx("140");
                //filter.AddEx("142");
                //filter.AddEx("150");
                //filter.AddEx("65213"); //Issue For Production
                //filter.AddEx("1470000002"); //Bin Master Data
                //filter.AddEx("1470000152"); // Pick Ticket 
                //filter.AddEx("60091"); //AR Reserve Invoice
                //filter.AddEx("60092"); //AP Reserve Invoice
                //filter.AddEx("1250000940");
                //filter.AddEx("143");
                //filter.AddEx("TranSelect.Forms.Settings");
                //filter.AddEx("426");
                //filter.AddEx("170");

                //filter = filters.Add(SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE);
                //filter.AddEx("62"); //Warehouse
                //filter.AddEx("134"); //Business Partner
                //filter.AddEx("139");
                //filter.AddEx("1474000001"); //InventoryCounting
                //filter.AddEx("140");
                //filter.AddEx("142");
                //filter.AddEx("150");
                //filter.AddEx("65213"); //Issue For Production
                //filter.AddEx("1470000002"); //Bin Master Data
                //filter.AddEx("60091"); //AR Reserve Invoice
                //filter.AddEx("60092"); //AP Reserve Invoice
                //filter.AddEx("65211"); //Production Order
                //filter.AddEx("85");
                //filter.AddEx("1250000940");
                //filter.AddEx("138");
                //filter.AddEx("143");
                //filter.AddEx("TranSelect.Forms.Settings");
                //filter.AddEx("426");
                //filter.AddEx("170");


                filter = filters.Add(SAPbouiCOM.BoEventTypes.et_FORM_LOAD);
                //filter.AddEx("150");
                //filter.AddEx("65211"); //Production Order
                //filter.AddEx("65213"); //Issue For Production
                //filter.AddEx("1470000002"); //Bin Master Data
                //filter.AddEx("142"); //PO
                //filter.AddEx("139");
                //filter.AddEx("1474000001"); //InventoryCounting
                //filter.AddEx("60091"); //AR Reserve Invoice
                //filter.AddEx("60092"); //AP Reserve Invoice
                //filter.AddEx("134"); //BP
                //filter.AddEx("85");
                //filter.AddEx("65214");
                //filter.AddEx("140");
                //filter.AddEx("1250000940");
                //filter.AddEx("62");
                //filter.AddEx("1470000152");
                //filter.AddEx("143");
                filter.AddEx("426");
                filter.AddEx("170");
                filter.AddEx("TranSelect.Forms.Settings");


                filter = filters.Add(SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE);
                //filter.AddEx("150");
                //filter.AddEx("426");
                filter.AddEx("TranSelect.Forms.Settings");


                //filter = filters.Add(SAPbouiCOM.BoEventTypes.et_FORM_CLOSE);
                //filter.AddEx("1470000152"); // Pick Ticket 
                //filter.AddEx("65213"); //Issue For Production
                //filter.AddEx("426");
                //filter.AddEx("170");
                //filter.AddEx("TranSelect.Forms.Settings");


                //filter = filters.Add(SAPbouiCOM.BoEventTypes.et_LOST_FOCUS);
                //filter.AddEx("1470000152"); // Pick Ticket
                //filter.AddEx("TranSelect.Forms.Settings");


                filter = filters.Add(SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST);
                //filter.AddEx("1470000002");
                filter.AddEx("TranSelect.Forms.Settings");


                //filter = filters.Add(SAPbouiCOM.BoEventTypes.et_MATRIX_LINK_PRESSED);
                //filter.AddEx("TranSelect.Forms.Settings");
                //filter.AddEx("426");
                //filter.AddEx("170");


                //filter = filters.Add(SAPbouiCOM.BoEventTypes.et_RIGHT_CLICK);
                //filter.AddEx("65211"); //Production Order
                //filter.AddEx("142");
                //filter.AddEx("TranSelect.Forms.Settings");

                //filter = filters.Add(SAPbouiCOM.BoEventTypes.et_VALIDATE);
                //filter.AddEx("426");
                //filter.AddEx("170");


                filter = filters.Add(SAPbouiCOM.BoEventTypes.et_LOST_FOCUS);
                filter.AddEx("170");
                filter.AddEx("426");


                filter = filters.Add(SAPbouiCOM.BoEventTypes.et_CLICK);
                filter.AddEx("426");
                filter.AddEx("170");


                filter = filters.Add(SAPbouiCOM.BoEventTypes.et_PRINT_LAYOUT_KEY);


                filter = filters.Add(SAPbouiCOM.BoEventTypes.et_FORM_RESIZE);


                filter = filters.Add(SAPbouiCOM.BoEventTypes.et_GOT_FOCUS);

                Globals.SBOApplication.SetFilter(filters);
            }
            catch (Exception ex)
            {
                Utils.LogError("Program.FilterEvents line " + Utils.GetLineException(ex) + " Error:", ex);
            }
        }


    }
}
