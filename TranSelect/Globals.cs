﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TranSelect
{
    public class Globals
    {
        public static SAPbobsCOM.Company SBOCompany { get; set; }

        public static SAPbouiCOM.Application SBOApplication { get; set; }

        public static string ItemCodeAj { get; set; }
        public static string ItemCodeAjOut { get; set; }
        public static string POIIP { get; set; }
        public static string POIOP { get; set; }

        public static char separator { get; set; }

        public static bool AddonFail { get; set; }

        static ResourceManager _dbQuery;
        public static ResourceManager DbQuery
        {
            get { return Globals._dbQuery; }
            set { Globals._dbQuery = value; }
        }

        public static bool ChooseDbQueryourceFile()
        {

            try
            {
                switch (Globals.SBOCompany.DbServerType)
                {
                    case SAPbobsCOM.BoDataServerTypes.dst_HANADB:
                        DbQuery = new ResourceManager("TranSelect.DBQuery.HANA", typeof(Globals).Assembly);
                        break;
                    case SAPbobsCOM.BoDataServerTypes.dst_MSSQL2005:
                    case SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008:
                    case SAPbobsCOM.BoDataServerTypes.dst_MSSQL2012:
                    case SAPbobsCOM.BoDataServerTypes.dst_MSSQL2014:
                    case SAPbobsCOM.BoDataServerTypes.dst_MSSQL2016:
                        DbQuery = new ResourceManager("TranSelect.DBQuery.SQL", typeof(Globals).Assembly);
                        break;
                    default:
                        DbQuery = new ResourceManager("TranSelect.DBQuery.SQL", typeof(Globals).Assembly);
                        break;
                }

                return true;
            }
            catch (Exception ex)
            {
                Utils.LogError("Globals.ChooseDbQueryourceFile.Line" + Utils.GetLineException(ex) + ".Ex:", ex);
                return false;
            }
        }

        public static void SetVariables()
        {
            try
            {
                var settings = SBOCompany.UserTables.Item("VK_GENADJ_SETTINGS");

                separator = Convert.ToChar(Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator);

                settings.GetByKey("1");

                if (settings.Code != "")
                {
                    ItemCodeAj = settings.UserFields.Fields.Item("U_ItemCode").Value.ToString();
                    ItemCodeAjOut = settings.UserFields.Fields.Item("U_ItemCodeOut").Value.ToString();
                    POIOP = settings.UserFields.Fields.Item("U_POIOP").Value.ToString();
                    POIIP = settings.UserFields.Fields.Item("U_POIIP").Value.ToString();
                }
                else
                {
                    AddonFail = false;
                }
            }
            catch (Exception ex)
            {
                Utils.LogError("Globals.SetVariables.Line" + Utils.GetLineException(ex) + ".Ex:", ex);

            }
        }
    }
}
