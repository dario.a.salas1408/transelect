﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranSelect.Model;
using TranSelect.Properties;

namespace TranSelect
{
    public class Utils
    {
        public static void LogError(string msg, Exception ex, bool showStatusBar = true)
        {

            if (Globals.SBOApplication != null)
            {
                //Show status bar
                if (showStatusBar)
                    Globals.SBOApplication.StatusBar.SetText(msg + ". Exception. " + ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
            }
        }

        public static void LogError(string msg, bool showStatusBar = true)
        {
            if (showStatusBar)
                Globals.SBOApplication.StatusBar.SetText(msg, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
        }

        public static void ReleaseObject(Object obj)
        {
            if (obj == null)
                return;
            try
            {
                if (System.Runtime.InteropServices.Marshal.IsComObject(obj))
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
            }
            catch (Exception ex)
            {
                LogError("Error starting WMS add-on", ex);
            }
            obj = null;
            GC.Collect();
            return;
        }

        public static int GetLineException(Exception ex)
        {
            var st = new StackTrace(ex, true);

            var frame = st.GetFrame(0);

            var line = frame.GetFileLineNumber();

            return line;
        }

        public static void LoadXmlObject(string obj)
        {
            System.Xml.XmlDocument xmldoc;
            string xmlstring;
            try
            {
                xmldoc = new System.Xml.XmlDocument();
                xmldoc.LoadXml(Resources.ResourceManager.GetObject(obj).ToString());
                xmlstring = xmldoc.OuterXml.ToString();
                Globals.SBOApplication.LoadBatchActions(ref xmlstring);
            }
            catch (Exception ex)
            {
                LogError("Utils.LoadXmlObject.Line" + GetLineException(ex) + ".Ex:", ex);
            }
        }

        public static void LogChanges(clsLogChange logChange)
        {
            var settings = Globals.SBOCompany.UserTables.Item("VK_GENADJ_LOGS");
            
            settings.UserFields.Fields.Item("U_PaymentCode").Value = logChange.PaymentCode;
            settings.UserFields.Fields.Item("U_PaymentType").Value = logChange.PaymentType;
            settings.UserFields.Fields.Item("U_IniRate").Value = logChange.IniRate.ToString();
            settings.UserFields.Fields.Item("U_InsTotal").Value = logChange.InsTotal.ToString();
            settings.UserFields.Fields.Item("U_BalDueDeb").Value = logChange.BalDueDeb.ToString();
            settings.UserFields.Fields.Item("U_ReconSum").Value = logChange.ReconSum.ToString();
            settings.UserFields.Fields.Item("U_Debit").Value = logChange.Debit.ToString();
            settings.UserFields.Fields.Item("U_Balance").Value = logChange.Balance.ToString();
            settings.UserFields.Fields.Item("U_LineCode").Value = logChange.LineCode; 
            settings.UserFields.Fields.Item("U_Status").Value = logChange.Status;
            settings.UserFields.Fields.Item("U_TableUpdated").Value = logChange.TableUpdated;
            settings.UserFields.Fields.Item("U_Date").Value = System.DateTime.Now.ToString();
            settings.UserFields.Fields.Item("U_UpdatedDo").Value = logChange.UpdatedDo;
            settings.UserFields.Fields.Item("U_SYSDeb").Value = logChange.SYSDeb.ToString();
            settings.UserFields.Fields.Item("U_SYSCred").Value = logChange.SYSCred.ToString();
            settings.UserFields.Fields.Item("U_Credit").Value = logChange.Credit.ToString();
            settings.UserFields.Fields.Item("U_Cancel").Value = logChange.Cancel.ToString();


            settings.Add();
        }
    }
}
