﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranSelect
{
    public class DbStructure
    {
        public static void Ini()
        {
            OVPM();
            VPM2();
            ORCT();
            RCT2();

            SettingsTable();
            LogTable();
        }

        private static void OVPM()
        {
            SAPbobsCOM.UserFieldsMD uf;
            string mod = null;
            try
            {
                mod = "OVPM";
                uf = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields) as SAPbobsCOM.UserFieldsMD;
                uf.TableName = "OVPM";
                uf.Name = "GeneralRate";
                uf.Description = "General Rate";
                uf.Type = SAPbobsCOM.BoFieldTypes.db_Float;
                uf.SubType = SAPbobsCOM.BoFldSubTypes.st_Rate;
                uf.DefaultValue = "0";
                UDFAdd(ref uf, mod);

                mod = "OVPM";
                uf = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields) as SAPbobsCOM.UserFieldsMD;
                uf.TableName = "OVPM";
                uf.Name = "DifferenceRate";
                uf.Description = "Difference Rate selected";
                uf.Type = SAPbobsCOM.BoFieldTypes.db_Float;
                uf.SubType = SAPbobsCOM.BoFldSubTypes.st_Sum;
                uf.DefaultValue = "0";
                UDFAdd(ref uf, mod);

                MensjCreateStructure("OVPM", true);

            }
            catch (Exception ex)
            {
                Utils.LogError("DbStructure.ORCT.Line" + Utils.GetLineException(ex) + ".Ex:", ex);
            }
        }

        private static void ORCT()
        {
            SAPbobsCOM.UserFieldsMD uf;
            string mod = null;
            try
            {
                mod = "ORCT";
                uf = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields) as SAPbobsCOM.UserFieldsMD;
                uf.TableName = "ORCT";
                uf.Name = "GeneralRate";
                uf.Description = "General Rate";
                uf.Type = SAPbobsCOM.BoFieldTypes.db_Float;
                uf.SubType = SAPbobsCOM.BoFldSubTypes.st_Rate;
                uf.DefaultValue = "0";
                UDFAdd(ref uf, mod);

                MensjCreateStructure("ORCT", true);

            }
            catch (Exception ex)
            {
                Utils.LogError("DbStructure.ORCT.Line" + Utils.GetLineException(ex) + ".Ex:", ex);
            }
        }

        private static void RCT2()
        {
            SAPbobsCOM.UserFieldsMD uf;
            string mod = null;
            try
            {
                mod = "RCT2";
                uf = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields) as SAPbobsCOM.UserFieldsMD;
                uf.TableName = "RCT2";
                uf.Name = "DifferenceRate";
                uf.Description = "Monto a ajustar";
                uf.Type = SAPbobsCOM.BoFieldTypes.db_Float;
                uf.SubType = SAPbobsCOM.BoFldSubTypes.st_Sum;
                uf.DefaultValue = "0";
                UDFAdd(ref uf, mod);

                mod = "RCT2";
                uf = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields) as SAPbobsCOM.UserFieldsMD;
                uf.TableName = "RCT2";
                uf.Name = "DiffRateOri";
                uf.Description = "Cotización Original";
                uf.Type = SAPbobsCOM.BoFieldTypes.db_Float;
                uf.SubType = SAPbobsCOM.BoFldSubTypes.st_Sum;
                uf.DefaultValue = "0";
                UDFAdd(ref uf, mod);

                mod = "RCT2";
                uf = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields) as SAPbobsCOM.UserFieldsMD;
                uf.TableName = "RCT2";
                uf.Name = "RateOri";
                uf.Description = "Tipo de Cambio Inicial";
                uf.Type = SAPbobsCOM.BoFieldTypes.db_Float;
                uf.SubType = SAPbobsCOM.BoFldSubTypes.st_Sum;
                uf.DefaultValue = "0";
                UDFAdd(ref uf, mod);

                MensjCreateStructure("RCT2", true);

            }
            catch (Exception ex)
            {
                Utils.LogError("DbStructure.RCT2.Line" + Utils.GetLineException(ex) + ".Ex:", ex);
            }
        }

        private static void VPM2()
        {
            SAPbobsCOM.UserFieldsMD uf;
            string mod = null;
            try
            {
                mod = "VPM2";
                uf = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields) as SAPbobsCOM.UserFieldsMD;
                uf.TableName = "VPM2";
                uf.Name = "DifferenceRate";
                uf.Description = "Monto a ajustar";
                uf.Type = SAPbobsCOM.BoFieldTypes.db_Float;
                uf.SubType = SAPbobsCOM.BoFldSubTypes.st_Sum;
                uf.DefaultValue = "0";
                UDFAdd(ref uf, mod);

                mod = "VPM2";
                uf = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields) as SAPbobsCOM.UserFieldsMD;
                uf.TableName = "VPM2";
                uf.Name = "DiffRateOri";
                uf.Description = "Cotización Original";
                uf.Type = SAPbobsCOM.BoFieldTypes.db_Float;
                uf.SubType = SAPbobsCOM.BoFldSubTypes.st_Sum;
                uf.DefaultValue = "0";
                UDFAdd(ref uf, mod);

                mod = "VPM2";
                uf = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields) as SAPbobsCOM.UserFieldsMD;
                uf.TableName = "VPM2";
                uf.Name = "RateOri";
                uf.Description = "Tipo de Cambio Inicial";
                uf.Type = SAPbobsCOM.BoFieldTypes.db_Float;
                uf.SubType = SAPbobsCOM.BoFldSubTypes.st_Sum;
                uf.DefaultValue = "0";
                UDFAdd(ref uf, mod);

                mod = "OVPM";
                uf = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields) as SAPbobsCOM.UserFieldsMD;
                uf.TableName = "OVPM";
                uf.Name = "DifferenceRate";
                uf.Description = "Monto a ajustar";
                uf.Type = SAPbobsCOM.BoFieldTypes.db_Float;
                uf.SubType = SAPbobsCOM.BoFldSubTypes.st_Sum;
                uf.DefaultValue = "0";
                UDFAdd(ref uf, mod);


                MensjCreateStructure("VPM2", true);

            }
            catch (Exception ex)
            {
                Utils.LogError("DbStructure.VPM2.Line" + Utils.GetLineException(ex) + ".Ex:", ex);
            }
        }

        private static void SettingsTable()
        {
            string table = "VK_GENADJ_SETTINGS";
            SAPbobsCOM.UserFieldsMD oUserFieldsMD = default(SAPbobsCOM.UserFieldsMD);
            string ls_Module = null;
            try
            {
                //Exit if table cannot be created
                if (TableAdd(table, "Ajustes Generales", SAPbobsCOM.BoUTBTableType.bott_NoObject) == false)
                    return;
                ls_Module = "Ajustes Generales";

                //Field
                oUserFieldsMD = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields) as SAPbobsCOM.UserFieldsMD;
                oUserFieldsMD.TableName = table;
                oUserFieldsMD.Name = "ItemCode";
                oUserFieldsMD.Description = "ItemCode";
                oUserFieldsMD.Type = SAPbobsCOM.BoFieldTypes.db_Alpha;
                oUserFieldsMD.Size = 254;
                UDFAdd(ref oUserFieldsMD, ls_Module);

                //Field
                oUserFieldsMD = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields) as SAPbobsCOM.UserFieldsMD;
                oUserFieldsMD.TableName = table;
                oUserFieldsMD.Name = "ItemCodeOut";
                oUserFieldsMD.Description = "ItemCode for outgoing payment";
                oUserFieldsMD.Type = SAPbobsCOM.BoFieldTypes.db_Alpha;
                oUserFieldsMD.Size = 254;
                UDFAdd(ref oUserFieldsMD, ls_Module);

                //Field
                oUserFieldsMD = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields) as SAPbobsCOM.UserFieldsMD;
                oUserFieldsMD.TableName = table;
                oUserFieldsMD.Name = "POIIP";
                oUserFieldsMD.Description = "POI in Incoming Payment";
                oUserFieldsMD.Type = SAPbobsCOM.BoFieldTypes.db_Alpha;
                oUserFieldsMD.Size = 254;
                UDFAdd(ref oUserFieldsMD, ls_Module);

                //Field
                oUserFieldsMD = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields) as SAPbobsCOM.UserFieldsMD;
                oUserFieldsMD.TableName = table;
                oUserFieldsMD.Name = "POIOP";
                oUserFieldsMD.Description = "POI in Outgoing Payment";
                oUserFieldsMD.Type = SAPbobsCOM.BoFieldTypes.db_Alpha;
                oUserFieldsMD.Size = 254;
                UDFAdd(ref oUserFieldsMD, ls_Module);

                MensjCreateStructure("Tabla Configuracion");
            }
            catch (Exception ex)
            {
                Utils.LogError("DbStructure.SettingsTable.Line" + Utils.GetLineException(ex) + ".Ex:", ex);
            }
        }

        private static void LogTable()
        {
            string table = "VK_GENADJ_LOGS";
            SAPbobsCOM.UserFieldsMD oUserFieldsMD = default(SAPbobsCOM.UserFieldsMD);
            string ls_Module = null;
            try
            {
                //Exit if table cannot be created
                if (TableAdd(table, "Logs", SAPbobsCOM.BoUTBTableType.bott_NoObjectAutoIncrement) == false)
                    return;
                ls_Module = "Logs";

                //Field
                oUserFieldsMD = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields) as SAPbobsCOM.UserFieldsMD;
                oUserFieldsMD.TableName = table;
                oUserFieldsMD.Name = "Date";
                oUserFieldsMD.Description = "Date";
                oUserFieldsMD.Type = SAPbobsCOM.BoFieldTypes.db_Date;
                UDFAdd(ref oUserFieldsMD, ls_Module);


                //Field
                oUserFieldsMD = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields) as SAPbobsCOM.UserFieldsMD;
                oUserFieldsMD.TableName = table;
                oUserFieldsMD.Name = "PaymentCode";
                oUserFieldsMD.Description = "Payment Code";
                oUserFieldsMD.Type = SAPbobsCOM.BoFieldTypes.db_Alpha;
                oUserFieldsMD.Size = 50;
                UDFAdd(ref oUserFieldsMD, ls_Module);

                //Field
                oUserFieldsMD = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields) as SAPbobsCOM.UserFieldsMD;
                oUserFieldsMD.TableName = table;
                oUserFieldsMD.Name = "PaymentType";
                oUserFieldsMD.Description = "Payment Type";
                oUserFieldsMD.Type = SAPbobsCOM.BoFieldTypes.db_Alpha;
                oUserFieldsMD.Size = 50;
                UDFAdd(ref oUserFieldsMD, ls_Module);

                //Field
                oUserFieldsMD = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields) as SAPbobsCOM.UserFieldsMD;
                oUserFieldsMD.TableName = table;
                oUserFieldsMD.Name = "IniRate";
                oUserFieldsMD.Description = "IniRate";
                oUserFieldsMD.Type = SAPbobsCOM.BoFieldTypes.db_Float;
                oUserFieldsMD.SubType = SAPbobsCOM.BoFldSubTypes.st_Price;
                UDFAdd(ref oUserFieldsMD, ls_Module);
                

                //Field
                oUserFieldsMD = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields) as SAPbobsCOM.UserFieldsMD;
                oUserFieldsMD.TableName = table;
                oUserFieldsMD.Name = "InsTotal";
                oUserFieldsMD.Description = "InsTotal";
                oUserFieldsMD.Type = SAPbobsCOM.BoFieldTypes.db_Float;
                oUserFieldsMD.SubType = SAPbobsCOM.BoFldSubTypes.st_Price;
                UDFAdd(ref oUserFieldsMD, ls_Module);


                //Field
                oUserFieldsMD = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields) as SAPbobsCOM.UserFieldsMD;
                oUserFieldsMD.TableName = table;
                oUserFieldsMD.Name = "BalDueDeb";
                oUserFieldsMD.Description = "BalDueDeb";
                oUserFieldsMD.Type = SAPbobsCOM.BoFieldTypes.db_Float;
                oUserFieldsMD.SubType = SAPbobsCOM.BoFldSubTypes.st_Price;
                UDFAdd(ref oUserFieldsMD, ls_Module);

                //Field
                oUserFieldsMD = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields) as SAPbobsCOM.UserFieldsMD;
                oUserFieldsMD.TableName = table;
                oUserFieldsMD.Name = "ReconSum";
                oUserFieldsMD.Description = "ReconSum";
                oUserFieldsMD.Type = SAPbobsCOM.BoFieldTypes.db_Float;
                oUserFieldsMD.SubType = SAPbobsCOM.BoFldSubTypes.st_Price;
                UDFAdd(ref oUserFieldsMD, ls_Module);

                //Field
                oUserFieldsMD = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields) as SAPbobsCOM.UserFieldsMD;
                oUserFieldsMD.TableName = table;
                oUserFieldsMD.Name = "Debit";
                oUserFieldsMD.Description = "Debit";
                oUserFieldsMD.Type = SAPbobsCOM.BoFieldTypes.db_Float;
                oUserFieldsMD.SubType = SAPbobsCOM.BoFldSubTypes.st_Price;
                UDFAdd(ref oUserFieldsMD, ls_Module);

                //Field
                oUserFieldsMD = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields) as SAPbobsCOM.UserFieldsMD;
                oUserFieldsMD.TableName = table;
                oUserFieldsMD.Name = "Credit";
                oUserFieldsMD.Description = "Credit";
                oUserFieldsMD.Type = SAPbobsCOM.BoFieldTypes.db_Float;
                oUserFieldsMD.SubType = SAPbobsCOM.BoFldSubTypes.st_Price;
                UDFAdd(ref oUserFieldsMD, ls_Module);

                //Field
                oUserFieldsMD = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields) as SAPbobsCOM.UserFieldsMD;
                oUserFieldsMD.TableName = table;
                oUserFieldsMD.Name = "SYSCred";
                oUserFieldsMD.Description = "SYSCred";
                oUserFieldsMD.Type = SAPbobsCOM.BoFieldTypes.db_Float;
                oUserFieldsMD.SubType = SAPbobsCOM.BoFldSubTypes.st_Price;
                UDFAdd(ref oUserFieldsMD, ls_Module);

                //Field
                oUserFieldsMD = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields) as SAPbobsCOM.UserFieldsMD;
                oUserFieldsMD.TableName = table;
                oUserFieldsMD.Name = "SYSDeb";
                oUserFieldsMD.Description = "SYSDeb";
                oUserFieldsMD.Type = SAPbobsCOM.BoFieldTypes.db_Float;
                oUserFieldsMD.SubType = SAPbobsCOM.BoFldSubTypes.st_Price;
                UDFAdd(ref oUserFieldsMD, ls_Module);


                //Field
                oUserFieldsMD = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields) as SAPbobsCOM.UserFieldsMD;
                oUserFieldsMD.TableName = table;
                oUserFieldsMD.Name = "Balance";
                oUserFieldsMD.Description = "Balance";
                oUserFieldsMD.Type = SAPbobsCOM.BoFieldTypes.db_Float;
                oUserFieldsMD.SubType = SAPbobsCOM.BoFldSubTypes.st_Price;
                UDFAdd(ref oUserFieldsMD, ls_Module);

                //Field
                oUserFieldsMD = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields) as SAPbobsCOM.UserFieldsMD;
                oUserFieldsMD.TableName = table;
                oUserFieldsMD.Name = "LineCode";
                oUserFieldsMD.Description = "Line in Code";
                oUserFieldsMD.Type = SAPbobsCOM.BoFieldTypes.db_Alpha;
                oUserFieldsMD.Size = 15;
                UDFAdd(ref oUserFieldsMD, ls_Module);

                //Field
                oUserFieldsMD = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields) as SAPbobsCOM.UserFieldsMD;
                oUserFieldsMD.TableName = table;
                oUserFieldsMD.Name = "TableUpdated";
                oUserFieldsMD.Description = "Table Updated";
                oUserFieldsMD.Type = SAPbobsCOM.BoFieldTypes.db_Alpha;
                oUserFieldsMD.Size = 25;
                UDFAdd(ref oUserFieldsMD, ls_Module);

                //Field
                oUserFieldsMD = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields) as SAPbobsCOM.UserFieldsMD;
                oUserFieldsMD.TableName = table;
                oUserFieldsMD.Name = "Status";
                oUserFieldsMD.Description = "Status";
                oUserFieldsMD.Type = SAPbobsCOM.BoFieldTypes.db_Alpha;
                oUserFieldsMD.Size = 3;
                UDFAdd(ref oUserFieldsMD, ls_Module);

                //Field
                oUserFieldsMD = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields) as SAPbobsCOM.UserFieldsMD;
                oUserFieldsMD.TableName = table;
                oUserFieldsMD.Name = "UpdatedDo";
                oUserFieldsMD.Description = "Updated Do";
                oUserFieldsMD.Type = SAPbobsCOM.BoFieldTypes.db_Memo;
                UDFAdd(ref oUserFieldsMD, ls_Module);

                //Field
                oUserFieldsMD = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields) as SAPbobsCOM.UserFieldsMD;
                oUserFieldsMD.TableName = table;
                oUserFieldsMD.Name = "Cancel";
                oUserFieldsMD.Description = "Cancel";
                oUserFieldsMD.Type = SAPbobsCOM.BoFieldTypes.db_Alpha;
                oUserFieldsMD.Size = 10;
                UDFAdd(ref oUserFieldsMD, ls_Module);

                MensjCreateStructure("Tabla Logs");
            }
            catch (Exception ex)
            {
                Utils.LogError("DbStructure.Logs.Line" + Utils.GetLineException(ex) + ".Ex:", ex);
            }
        }

        private static bool TableAdd(string name, string desc, SAPbobsCOM.BoUTBTableType type)
        {
            bool ret = false;
            SAPbobsCOM.UserTablesMD ut = default(SAPbobsCOM.UserTablesMD);
            int lRetCode = 0;
            try
            {
                ut = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserTables) as SAPbobsCOM.UserTablesMD;
                if (ut.GetByKey(name) == false)
                {
                    ut.TableName = name;
                    ut.TableDescription = desc;
                    ut.TableType = type;
                    lRetCode = ut.Add();
                    if (lRetCode != 0)
                    {
                        Utils.LogError("Create Table" + name + " - " + Globals.SBOCompany.GetLastErrorCode() + " - " + Globals.SBOCompany.GetLastErrorDescription());
                        ret = false;
                    }
                    else
                    {
                        ret = true;
                    }
                }
                else
                {
                    ret = true;
                }
                Utils.ReleaseObject(ut);
            }
            catch (Exception ex)
            {
                Utils.LogError("DbStructure.TableAdd.Line" + Utils.GetLineException(ex) + ".Ex:", ex);
            }
            return ret;
        }

        private static void UDFAdd(ref SAPbobsCOM.UserFieldsMD userField, string module)
        {
            int li_ret = 0;
            try
            {
                if (UDFCheck(userField.Name, userField.TableName) == false)
                {
                    li_ret = userField.Add();
                    if (li_ret != 0)
                    {
                        Utils.LogError(module + ". Field: " + userField.Name + " - " + Globals.SBOCompany.GetLastErrorCode() + " - " + Globals.SBOCompany.GetLastErrorDescription());
                    }
                }
                Utils.ReleaseObject(userField);
            }
            catch (Exception ex)
            {
                Utils.LogError("DbStructure.UDFAdd.Line" + Utils.GetLineException(ex) + ".Ex:", ex);
            }
        }

        public static bool UDFCheck(string uDF, string tableName)
        {
            bool functionReturnValue = false;
            SAPbobsCOM.Recordset oRecordSet = default(SAPbobsCOM.Recordset);
            try
            {
                oRecordSet = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                try
                {
                    //String ls_sql = string.Format("SELECT FieldID FROM CUFD WHERE TableID = '{0}' AND AliasID = '{1}'", tableName, uDF);
                    String ls_sql = string.Format(Globals.DbQuery.GetString("Udfs01"), tableName, uDF); 
                    oRecordSet.DoQuery(ls_sql);

                    if (oRecordSet.RecordCount > 0)
                    {
                        functionReturnValue = true;
                    }
                    else
                    {
                        functionReturnValue = false;
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.Message);
                    functionReturnValue = false;
                }
                Utils.ReleaseObject(oRecordSet);
            }
            catch (Exception ex)
            {
                Utils.LogError("DbStructure.UDFCheck.Line" + Utils.GetLineException(ex) + ".Ex:", ex);
            }
            return functionReturnValue;
        }

        private static bool UDOCheck(string code)
        {
            bool functionReturnValue = false;
            SAPbobsCOM.UserObjectsMD oUserObjectMD = default(SAPbobsCOM.UserObjectsMD);
            try
            {
                oUserObjectMD = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD) as SAPbobsCOM.UserObjectsMD;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
                //Return true if error. Means the user doesn't have authorization to create objects.
                return true;
            }
            //Get UDO object if exists, and return that boolean
            functionReturnValue = oUserObjectMD.GetByKey(code);
            //Release object
            Utils.ReleaseObject(oUserObjectMD);
            return functionReturnValue;
        }

        private static void UDOAdd(ref SAPbobsCOM.UserObjectsMD uo, string module)
        {
            int li_ret = 0;
            try
            {
                li_ret = uo.Add();
                if (li_ret != 0)
                {
                    Utils.LogError(module + ". ObjectName: " + uo.Name + " - " + Globals.SBOCompany.GetLastErrorCode() + " - " + Globals.SBOCompany.GetLastErrorDescription());
                }
                Utils.ReleaseObject(uo);
            }
            catch (Exception ex)
            {
                Utils.LogError("DbStructure.UDOAdd.Line" + Utils.GetLineException(ex) + ".Ex:", ex);
            }
        }

        private static void MensjCreateStructure(string table, bool udf = false)
        {
            string type = (udf ? "UDFs" : "table");

            Globals.SBOApplication.StatusBar.SetText($"Created {type} {table}", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
        }
    }
}
