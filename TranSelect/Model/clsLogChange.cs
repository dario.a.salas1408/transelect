﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranSelect.Model
{
    public class clsLogChange
    {
        public clsLogChange()
        {
            IniRate = 0;
            InsTotal = 0;
            BalDueDeb = 0;
            ReconSum = 0;
            Debit = 0;
            SYSCred = 0;
            Balance = 0;
            PaymentCode = "";
            PaymentType = "";
            Status = "";
            LineCode = "";
            TableUpdated = "";
            UpdatedDo = "";
            SYSDeb = 0;
            Credit = 0;
            Cancel = "N";
        }

        public string PaymentCode { get; set; }
        public string PaymentType { get; set; }
        public decimal IniRate { get; set; }
        public decimal InsTotal { get; set; }
        public decimal BalDueDeb { get; set; }
        public decimal ReconSum { get; set; }
        public decimal Debit { get; set; }
        public decimal SYSCred { get; set; }
        public decimal Balance { get; set; }
        public string LineCode { get; set; }
        public string TableUpdated { get; set; }
        public string Status { get; set; }
        public string UpdatedDo { get; set; }
        public decimal SYSDeb { get; set; }
        public decimal Credit { get; set; }
        public string Cancel { get; set; }

    }
}
