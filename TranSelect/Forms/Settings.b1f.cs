﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;
using SAPbouiCOM;

namespace TranSelect.Forms
{
    [FormAttribute("TranSelect.Forms.Settings", "Forms/Settings.b1f")]
    class Settings : UserFormBase
    {
        private static SAPbouiCOM.Button Button0;
        private static SAPbouiCOM.Button Button1;
        private static SAPbouiCOM.EditText EditText0;
        private static SAPbouiCOM.StaticText StaticText0;
        private static SAPbouiCOM.EditText EditText1;
        private static SAPbouiCOM.StaticText StaticText1;
        private static SAPbouiCOM.EditText EditText2;
        private static SAPbouiCOM.StaticText StaticText2;
        private static SAPbouiCOM.EditText EditText3;  
        private IForm ThisForm { get { return UIAPIRawForm; } }

        public Settings()
        {
        }

        public static void Event(string formUID, ref SAPbouiCOM.ItemEvent itemEvent, out bool bubbleEvent)
        {
            bubbleEvent = true;
            try
            {
                switch (itemEvent.EventType)
                {
                    case SAPbouiCOM.BoEventTypes.et_FORM_LOAD:

                        if (itemEvent.BeforeAction == false)
                        {
                            if (itemEvent.ActionSuccess == true)
                            {
                                
                            }
                        }
                        
                        break;
                    case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST:
                        EventChooseFromList(formUID, ref itemEvent, out bubbleEvent);
                        break;

                    case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED:
                        ItemPressed(formUID, ref itemEvent, out bubbleEvent);
                        break;
                }

            }
            catch (Exception ex)
            {
                Utils.LogError("Pagos_efectuados.Event.Line" + Utils.GetLineException(ex) + ".Ex:", ex);
            }
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.OnCustomInitialize();
        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }

        private void OnCustomInitialize()
        {


            EditText0 = ((SAPbouiCOM.EditText)(this.GetItem("Item_2").Specific));
            StaticText0 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_3").Specific));
            EditText1 = ((SAPbouiCOM.EditText)(this.GetItem("Item_4").Specific));
            StaticText1 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_5").Specific));
            EditText2 = ((SAPbouiCOM.EditText)(this.GetItem("Item_6").Specific));
            StaticText2 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_7").Specific));
            EditText3 = ((SAPbouiCOM.EditText)(this.GetItem("Item_0").Specific)); 
            
            ThisForm.DataSources.UserDataSources.Add("sItemCode", BoDataType.dt_SHORT_TEXT, 254);
            ThisForm.DataSources.UserDataSources.Add("sItemCodeO", BoDataType.dt_SHORT_TEXT, 254);

            EditText0.DataBind.SetBound(true, "", "sItemCode");
            EditText3.DataBind.SetBound(true, "", "sItemCodeO");

            AddChooseFromList();
            AddChooseFromListOut();

            //  Adding Choose From List
            EditText0.DataBind.SetBound(true, "", "sItemCode");
            EditText3.DataBind.SetBound(true, "", "sItemCodeO");

            //  Adding 2 Choose From List Object, ONE FOR EDIT TEXT AND ONE FOR BUTTON.
            EditText0.ChooseFromListUID = "CFL1";
            EditText3.ChooseFromListUID = "CFL2";

            // We set the alias only after the UID is set, the alias is the field in the database
            // It compares the value in the edit text and narrows the CFL accrodingly
            EditText0.ChooseFromListAlias = "ItemCode";
            EditText3.ChooseFromListAlias = "ItemCode";

            var settings = Globals.SBOCompany.UserTables.Item("VK_GENADJ_SETTINGS");

            settings.GetByKey("1");

            if (settings.Code != "")
            {
                EditText0.Value = settings.UserFields.Fields.Item("U_ItemCode").Value.ToString();
                EditText2.Value = settings.UserFields.Fields.Item("U_POIOP").Value.ToString();
                EditText1.Value = settings.UserFields.Fields.Item("U_POIIP").Value.ToString();
                EditText3.Value = settings.UserFields.Fields.Item("U_ItemCodeOut").Value.ToString();
            }

        }

        private void AddChooseFromList()
        {
            try
            {

                SAPbouiCOM.ChooseFromListCollection oCFLs = null;
                SAPbouiCOM.Conditions oCons = null;
                SAPbouiCOM.Condition oCon = null;

                oCFLs = ThisForm.ChooseFromLists;

                SAPbouiCOM.ChooseFromList oCFL = null;
                SAPbouiCOM.ChooseFromListCreationParams oCFLCreationParams = null;
                oCFLCreationParams = ((SAPbouiCOM.ChooseFromListCreationParams)(Globals.SBOApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams)));

                oCFLCreationParams.MultiSelection = false;
                oCFLCreationParams.ObjectType = "4";
                oCFLCreationParams.UniqueID = "CFL1";

                oCFL = oCFLs.Add(oCFLCreationParams);

            }
            catch
            {
                //Interaction.MsgBox(Information.Err().Description, (Microsoft.VisualBasic.MsgBoxStyle)(0), null);
            }
        }

        private void AddChooseFromListOut()
        {
            try
            {

                SAPbouiCOM.ChooseFromListCollection oCFLs = null;
                SAPbouiCOM.Conditions oCons = null;
                SAPbouiCOM.Condition oCon = null;

                oCFLs = ThisForm.ChooseFromLists;

                SAPbouiCOM.ChooseFromList oCFL = null;
                SAPbouiCOM.ChooseFromListCreationParams oCFLCreationParams = null;
                oCFLCreationParams = ((SAPbouiCOM.ChooseFromListCreationParams)(Globals.SBOApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams)));

                oCFLCreationParams.MultiSelection = false;
                oCFLCreationParams.ObjectType = "4";
                oCFLCreationParams.UniqueID = "CFL2";

                oCFL = oCFLs.Add(oCFLCreationParams);

            }
            catch
            {
                //Interaction.MsgBox(Information.Err().Description, (Microsoft.VisualBasic.MsgBoxStyle)(0), null);
            }
        }

        private static void EventChooseFromList(string formUID, ref SAPbouiCOM.ItemEvent itemEvent, out bool bubbleEvent)
        {
            bubbleEvent = true;
            Form form;
            IChooseFromListEvent oCFLEvento;
            DataTable mDataTable;
            DataTable mDataTableAll;
            try
            {
                form = Globals.SBOApplication.Forms.Item(formUID);

                if (itemEvent.BeforeAction == false)
                {
                    if (itemEvent.ActionSuccess == true)
                    {
                        switch (itemEvent.ItemUID)
                        {
                            case "Item_2":
                                oCFLEvento = (SAPbouiCOM.IChooseFromListEvent)itemEvent;
                                mDataTable = oCFLEvento.SelectedObjects;

                                if (mDataTable == null) return;

                                string code = mDataTable.GetValue("ItemCode", 0).ToString();

                                form.Items.Item("Item_4").Click();
                                form.Items.Item("Item_2").Enabled = false;
                                (form.Items.Item("Item_2").Specific as SAPbouiCOM.EditText).String = code;
                                form.Items.Item("Item_2").Enabled = true;

                                break;

                            case "Item_0":
                                oCFLEvento = (SAPbouiCOM.IChooseFromListEvent)itemEvent;
                                mDataTable = oCFLEvento.SelectedObjects;

                                if (mDataTable == null) return;

                                string codeOut = mDataTable.GetValue("ItemCode", 0).ToString();

                                form.Items.Item("Item_4").Click();
                                form.Items.Item("Item_0").Enabled = false;
                                (form.Items.Item("Item_0").Specific as SAPbouiCOM.EditText).String = codeOut;
                                form.Items.Item("Item_0").Enabled = true;

                                break;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Utils.LogError("EventChooseFromList", ex);
            }
        }

        private static void ItemPressed(string formUID, ref SAPbouiCOM.ItemEvent itemEvent, out bool bubbleEvent)
        {
            bubbleEvent = true;

            try
            {
                if (itemEvent.BeforeAction == false)
                {
                    if (itemEvent.ActionSuccess == true)
                    {
                        switch (itemEvent.ItemUID)
                        {
                            case "btnGuardar":
                                var settings = Globals.SBOCompany.UserTables.Item("VK_GENADJ_SETTINGS");
                                //settings.GetByKey("1");

                                settings.UserFields.Fields.Item("U_ItemCode").Value = EditText0.Value.ToString();
                                settings.UserFields.Fields.Item("U_POIIP").Value = EditText1.Value.ToString();
                                settings.UserFields.Fields.Item("U_POIOP").Value = EditText2.Value.ToString();
                                settings.UserFields.Fields.Item("U_ItemCodeOut").Value = EditText3.Value.ToString();

                                if (settings.Code != "")
                                {
                                    settings.Update();
                                }
                                else
                                {
                                    settings.Code = "1";
                                    settings.Name = "Main";
                                    settings.Add();
                                }

                                Globals.POIIP = EditText1.Value.ToString();
                                Globals.POIOP = EditText2.Value.ToString();
                                Globals.ItemCodeAj = EditText0.Value.ToString();
                                Globals.ItemCodeAjOut = EditText3.Value.ToString();
                                Globals.SBOApplication.StatusBar.SetText("Los datos se guardaron correctamente!", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
                                break;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Utils.LogError("EventChooseFromList", ex);
            }
        }




    }
}
