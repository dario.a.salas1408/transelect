
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;
using SAPbouiCOM;
using System.Threading;

namespace TranSelect
{

    [FormAttribute("170", "SAPForms/Pagos recibidos.b1f")]
    class Pagos_recibidos : SystemFormBase
    {
        private static decimal rate;
        private static decimal generalRate;
        private static List<int> OrdersToProcess;
        private static string formID;
        private static int documentCreated;
        private static string typeDocumentCreated;
        private Button Button1;
        private StaticText StaticText0;
        private EditText EditText0;
        private static bool reloadGrid = false;
        private StaticText StaticText2;
        private EditText EditText1;

        public Pagos_recibidos()
        {
            OrdersToProcess = new List<int>();
            documentCreated = 0;
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.iniComponent();

            this.OnCustomInitialize();

        }

        private void OnCustomInitialize()
        {

        }

        private void iniComponent()
        {
            this.Button1 = ((SAPbouiCOM.Button)(this.GetItem("Item_1").Specific));
            //this.Button1.PressedBefore += new SAPbouiCOM._IButtonEvents_PressedBeforeEventHandler(this.Button1_PressedBefore);
            //this.Button1.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button1_ClickBefore);
            this.StaticText0 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_0").Specific));
            this.EditText0 = ((SAPbouiCOM.EditText)(this.GetItem("Item_2").Specific));
            this.StaticText2 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_3").Specific));
            this.EditText1 = ((SAPbouiCOM.EditText)(this.GetItem("Item_4").Specific));

            EditText0.DataBind.SetBound(true, "ORCT", "U_GeneralRate");
            EditText1.DataBind.SetBound(true, "ORCT", "U_DifferenceRate");

            SAPbobsCOM.SBObob sBObob = (SAPbobsCOM.SBObob)Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoBridge);

            var rates = sBObob.GetCurrencyRate("USD", System.DateTime.Now);

            while (!rates.EoF)
            {
                rate = Convert.ToDecimal(rates.Fields.Item(0).Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator));
                rates.MoveNext();
            }
            this.EditText1.Item.Enabled = false;
        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }

        public static void Event(string formUID, ref SAPbouiCOM.ItemEvent itemEvent, out bool bubbleEvent)
        {
            bubbleEvent = true;
            formID = formUID;
            try
            {
                switch (itemEvent.EventType)
                {
                    case SAPbouiCOM.BoEventTypes.et_FORM_LOAD:
                        break;
                    case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST:
                        break;
                    case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED:
                        Pressed(ref formUID, ref itemEvent, out bubbleEvent);
                        break;
                    case SAPbouiCOM.BoEventTypes.et_CLICK:
                        switch (itemEvent.ItemUID)
                        {
                            case "20":
                                if (itemEvent.ColUID == "10000127")
                                {
                                    GrillaClick(ref formUID, ref itemEvent, out bubbleEvent);
                                }
                                break;
                            case "2":
                                Cancel(ref formUID, ref itemEvent, out bubbleEvent);
                                break;
                        }
                        break;
                    case SAPbouiCOM.BoEventTypes.et_VALIDATE:
                        break;
                }

            }
            catch (Exception ex)
            {
                Utils.LogError("Pagos_efectuados.Event.Line" + Utils.GetLineException(ex) + ".Ex:", ex);
            }
        }

        private static void GrillaClick(ref String formUID, ref SAPbouiCOM.ItemEvent itemEvent, out bool bubbleEvent)
        {
            bubbleEvent = true;

            if (reloadGrid)
            {
                bubbleEvent = false;
                return;
            }

            Form form;
            Matrix matrix;
            form = Globals.SBOApplication.Forms.Item(formUID);
            matrix = (Matrix)form.Items.Item("20").Specific;
            form.Freeze(true);
            var bttnCalculate = (Button)form.Items.Item("Item_1").Specific;
            var cardCode = (EditText)form.Items.Item("5").Specific;
            decimal totalDoc = 0;
            string ls_sqlVal = string.Empty;

            for (int i = 1; i <= matrix.RowCount; i++)
            {
                var typeDocVal = (EditText)matrix.Columns.Item("45").Cells.Item(i).Specific;
                var orderNumVal = (EditText)matrix.Columns.Item("1").Cells.Item(i).Specific;
                var selectedCheckBoxVal = (CheckBox)matrix.Columns.Item("10000127").Cells.Item(i).Specific;
                SAPbobsCOM.Recordset oRecordsetVal = null;
                oRecordsetVal = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                var udfDiff = (EditText)matrix.Columns.Item("U_DifferenceRate").Cells.Item(i).Specific;
                var difRateTxt = (EditText)form.Items.Item("Item_4").Specific;
                udfDiff.Value = "0.00";
                EditText generalRateTxt;
                generalRateTxt = (EditText)form.Items.Item("Item_2").Specific;
                string amountGeneralRate = string.Empty;
                amountGeneralRate = generalRateTxt.Value.Replace(',', Globals.separator).Replace('.', Globals.separator);
                amountGeneralRate = (amountGeneralRate == "" ? "0,0".Replace(',', Globals.separator).Replace('.', Globals.separator) : amountGeneralRate);
                EditText amountTxtDUE;
                EditText amountTxtHolding;
                string amountDUE = string.Empty;
                string amountHolding = string.Empty;
                amountTxtDUE = (EditText)form.Items.Item("45").Specific;
                amountTxtHolding = (EditText)form.Items.Item("253000195").Specific;
                decimal amountToOP;

                if (amountTxtDUE.Value != "")
                {
                    if (amountTxtDUE.Value.Length > 0)
                    {
                        amountDUE = amountTxtDUE.Value.Substring(0, amountTxtDUE.Value.Length - 3);
                        amountDUE = amountDUE.Replace(',', Globals.separator).Replace('.', Globals.separator);
                    }
                    else
                    {
                        amountDUE = "0";
                    }
                    
                    if (amountTxtHolding.Value.Length > 0)
                    {
                        amountHolding = amountTxtHolding.Value.Substring(0, amountTxtHolding.Value.Length - 3);
                        amountHolding = amountHolding.Replace(',', Globals.separator).Replace('.', Globals.separator);
                    }
                    else
                    {
                        amountHolding = "0";
                    }
                }
                else
                {
                    amountHolding = "0";
                    amountDUE = "0";
                }

                amountToOP = Convert.ToDecimal(amountDUE) + Convert.ToDecimal(amountHolding);

                if (selectedCheckBoxVal.Checked == true)
                {
                    if (typeDocVal.Value == "13")
                    {
                        ls_sqlVal = string.Format(Globals.DbQuery.GetString("IncPay01"), orderNumVal.Value, cardCode.Value);
                    }
                    else
                    {
                        ls_sqlVal = string.Format(Globals.DbQuery.GetString("IncPay02"), orderNumVal.Value, cardCode.Value);
                    }

                    oRecordsetVal.DoQuery(ls_sqlVal);

                    if (!oRecordsetVal.EoF)
                    {
                        OrdersToProcess.Add(Convert.ToInt32(orderNumVal.Value));

                        while (!oRecordsetVal.EoF)
                        {
                            if (oRecordsetVal.Fields.Item("DocCur").Value.ToString() == "USD")
                            {

                                decimal totalLine = 0;

                                decimal totalLineWithCurrentRate = 0;

                                decimal rateToApply = rate;

                                decimal rateToApplyLine = Convert.ToDecimal(oRecordsetVal.Fields.Item("DocRate").Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator));

                                if (amountGeneralRate != ("0,0".Replace(',', Globals.separator).Replace('.', Globals.separator)))
                                {
                                    rateToApplyLine = Convert.ToDecimal(amountGeneralRate);

                                }

                                totalLine = Convert.ToDecimal(oRecordsetVal.Fields.Item("DocTotalFC").Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator)) * rateToApplyLine;

                                totalLine = typeDocVal.Value == "13" ? (totalLine * 1) : (totalLine * -1);

                                totalDoc = totalDoc + totalLine;

                                totalLineWithCurrentRate = Convert.ToDecimal(oRecordsetVal.Fields.Item("DocTotalFC").Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator)) * rateToApply;

                                totalLineWithCurrentRate = typeDocVal.Value == "13" ? (totalLineWithCurrentRate * 1) : (totalLineWithCurrentRate * -1);

                                totalLine = totalLine - totalLineWithCurrentRate;

                                udfDiff.Value = totalLine.ToString().Replace(",", ".");
                            }
                            else
                            {
                                string ls_sql = string.Empty;

                                decimal totalLineWithCurrentRate = 0;

                                if (typeDocVal.Value == "13")
                                {
                                    ls_sql = string.Format(Globals.DbQuery.GetString("IncPay01"), orderNumVal.Value, cardCode.Value);
                                }
                                else
                                {
                                    ls_sql = string.Format(Globals.DbQuery.GetString("IncPay02"), orderNumVal.Value, cardCode.Value);
                                }

                                SAPbobsCOM.Recordset oRecordset = null;
                                oRecordset = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;

                                oRecordsetVal.DoQuery(ls_sqlVal);

                                totalLineWithCurrentRate = typeDocVal.Value == "13" ? (Convert.ToDecimal(oRecordsetVal.Fields.Item("DocTotal").Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator)) * 1) : (Convert.ToDecimal(oRecordsetVal.Fields.Item("DocTotal").Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator)) * -1);

                                totalDoc = totalDoc + totalLineWithCurrentRate;

                            }

                            oRecordsetVal.MoveNext();
                        }

                    }

                }
                else
                {
                    OrdersToProcess.Remove(Convert.ToInt32(orderNumVal.Value));
                }

                difRateTxt.Value = (totalDoc - amountToOP).ToString().Replace(",", ".");

                if ((totalDoc - amountToOP) != 0)
                {
                    bttnCalculate.Item.Enabled = true;
                }
                else
                {
                    bttnCalculate.Item.Enabled = false;
                }
            }

            form.Freeze(false);

        }

        private static void ReloadForm(string cardCode, int orderAj, string typeDocument, decimal totalAdj, out bool BubbleEvent)
        {
            BubbleEvent = false;
            reloadGrid = true;

            Form form;
            OptionBtn opVendor;
            OptionBtn opCustomer;
            EditText txtCardCode;
            Matrix matrix;
            Button bttnCalculate;

            int type = 13;
            try
            {
                form = Globals.SBOApplication.Forms.Item(formID);
                form.Freeze(true);
                opCustomer = (OptionBtn)form.Items.Item("56").Specific;
                opVendor = (OptionBtn)form.Items.Item("57").Specific;
                txtCardCode = (EditText)form.Items.Item("5").Specific;
                matrix = (Matrix)form.Items.Item("20").Specific;
                bttnCalculate = (Button)form.Items.Item("Item_1").Specific;
                type = (typeDocument == "Nota de Credito" ? 14 : 13);

                opCustomer.Item.Click();
                opVendor.Item.Click();
                opCustomer.Item.Click();
                txtCardCode.Value = cardCode;

                var difRateTxt = (EditText)form.Items.Item("Item_4").Specific;
                decimal diff = Convert.ToDecimal(difRateTxt.Value.Replace(',', Globals.separator).Replace('.', Globals.separator));
                EditText generalRateTxt;
                generalRateTxt = (EditText)form.Items.Item("Item_2").Specific;
                
                string ls_sqlVal = string.Empty;
                SAPbobsCOM.Recordset oRecordsetVal = null;
                oRecordsetVal = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                
                foreach (var item in OrdersToProcess.Distinct().ToList())
                {
                    for (int i = 1; i <= matrix.RowCount; i++)
                    {
                        var orderNum = (EditText)matrix.Columns.Item("1").Cells.Item(i).Specific;
                        var typeDoc = (EditText)matrix.Columns.Item("45").Cells.Item(i).Specific;
                        var udfDiff = (EditText)matrix.Columns.Item("U_DifferenceRate").Cells.Item(i).Specific;
                        
                        if (Convert.ToInt32(orderNum.Value) == item)
                        {
                            var selectedCheckBox = (CheckBox)matrix.Columns.Item("10000127").Cells.Item(i).Specific;
                            selectedCheckBox.Checked = true;
                            
                            if (typeDoc.Value == "13")
                            {
                                ls_sqlVal = string.Format(Globals.DbQuery.GetString("IncPay01"), item, cardCode);
                            }
                            else
                            {
                                ls_sqlVal = string.Format(Globals.DbQuery.GetString("IncPay02"), item, cardCode);
                            }

                            oRecordsetVal.DoQuery(ls_sqlVal);

                            if (!oRecordsetVal.EoF)
                            {

                                while (!oRecordsetVal.EoF)
                                {
                                    if (oRecordsetVal.Fields.Item("DocCur").Value.ToString() == "USD")
                                    {

                                        decimal totalLine = 0;

                                        decimal totalLineWithCurrentRate = 0;

                                        decimal rateToApply = rate;

                                        decimal rateToApplyLine = Convert.ToDecimal(oRecordsetVal.Fields.Item("DocRate").Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator));

                                        if (generalRate != 0)
                                        {
                                            rateToApplyLine = Convert.ToDecimal(generalRate);
                                        }

                                        totalLine = Convert.ToDecimal(oRecordsetVal.Fields.Item("DocTotalFC").Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator)) * rateToApplyLine;

                                        totalLine = typeDoc.Value == "13" ? (totalLine * 1) : (totalLine * -1);
                                        
                                        totalLineWithCurrentRate = Convert.ToDecimal(oRecordsetVal.Fields.Item("DocTotalFC").Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator)) * rateToApply;

                                        totalLineWithCurrentRate = typeDoc.Value == "13" ? (totalLineWithCurrentRate * 1) : (totalLineWithCurrentRate * -1);

                                        totalLine = totalLine - totalLineWithCurrentRate;

                                        udfDiff.Value = totalLine.ToString().Replace(",", ".");
                                    }
                                   

                                    oRecordsetVal.MoveNext();
                                }

                            }
                        }
                    }
                }

                difRateTxt.Item.Enabled = true;
                difRateTxt.Value = totalAdj.ToString().Replace(",", ".");
                generalRateTxt.Value = generalRate.ToString().Replace(",", ".");
                difRateTxt.Item.Enabled = false;

                SAPbobsCOM.Recordset oRecordsetGetDocNum = null;
                oRecordsetGetDocNum = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                String ls_sqlDocNum = string.Empty;

                if (type == 13)
                {
                    ls_sqlDocNum = string.Format(Globals.DbQuery.GetString("IncPay03"), orderAj, cardCode);
                }
                else
                {
                    ls_sqlDocNum = string.Format(Globals.DbQuery.GetString("IncPay04"), orderAj, cardCode);
                }

                oRecordsetGetDocNum.DoQuery(ls_sqlDocNum);

                while (!oRecordsetGetDocNum.EoF)
                {
                    orderAj = Convert.ToInt32(oRecordsetGetDocNum.Fields.Item("DocNum").Value);
                    oRecordsetGetDocNum.MoveNext();
                }

                for (int i = 1; i <= matrix.RowCount; i++)
                {
                    var orderNum = (EditText)matrix.Columns.Item("1").Cells.Item(i).Specific;
                    var typeDoc = (EditText)matrix.Columns.Item("45").Cells.Item(i).Specific;

                    if (Convert.ToInt32(orderNum.Value) == orderAj)
                    {
                        if (typeDoc.Value == "13")
                        {
                            SAPbobsCOM.Recordset oRecordset = null;
                            oRecordset = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                            String ls_sql = string.Format(Globals.DbQuery.GetString("IncPay05"), orderNum.Value, cardCode);
                            oRecordset.DoQuery(ls_sql);

                            if (!oRecordset.EoF)
                            {
                                var selectedCheckBox = (CheckBox)matrix.Columns.Item("10000127").Cells.Item(i).Specific;
                                selectedCheckBox.Checked = true;
                            }

                        }
                        else
                        {
                            var selectedCheckBox = (CheckBox)matrix.Columns.Item("10000127").Cells.Item(i).Specific;
                            selectedCheckBox.Checked = true;
                        }

                    }
                }
                reloadGrid = false;
                OrdersToProcess.Clear();
                bttnCalculate.Item.Enabled = false;
                form.Freeze(false);

            }
            catch (Exception ex)
            {
                Utils.LogError("Pagos_efectuados.ReloadForm" + Utils.GetLineException(ex) + ".Ex:", ex);
            }
        }

        private static void Pressed(ref String formUID, ref SAPbouiCOM.ItemEvent itemEvent, out bool bubbleEvent)
        {
            bubbleEvent = true;
            Form form;

            if (itemEvent.BeforeAction == false)
            {
                if (itemEvent.ActionSuccess == true)
                {
                    switch (itemEvent.ItemUID)
                    {
                        case "Item_1":

                            try
                            {
                                EditText cardCode;
                                EditText generalRateTxt;
                                string amountGeneralRate = string.Empty;
                                form = Globals.SBOApplication.Forms.Item(formID);
                                cardCode = (EditText)form.Items.Item("5").Specific;
                                generalRateTxt = (EditText)form.Items.Item("Item_2").Specific;

                                amountGeneralRate = generalRateTxt.Value.Replace(',', Globals.separator).Replace('.', Globals.separator);
                                amountGeneralRate = (amountGeneralRate == "" ? "0,0".Replace(',', Globals.separator).Replace('.', Globals.separator) : amountGeneralRate);

                                decimal differentAmount = 0;

                                var txtDiffRate = (EditText)form.Items.Item("Item_4").Specific;

                                differentAmount = Convert.ToDecimal(txtDiffRate.Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator));

                                if (differentAmount != 0)
                                {
                                    var typeDocument = (differentAmount > 0 ? "Nota de Debito" : "Nota de Credito");


                                    if (Globals.SBOApplication.MessageBox($"Hay una diferencia por el tipo de cambio en los documentos de {differentAmount.ToString().Replace(",", ".")}, se generará una {typeDocument} por este monto, para resolver este desajuste, desea realizarlo?", 2, "Yes", "No", "") == 1)
                                    {
                                        SAPbobsCOM.Documents doc;


                                        if (typeDocument == "Nota de Credito")
                                        {
                                            doc = (SAPbobsCOM.Documents)Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oCreditNotes);
                                            typeDocumentCreated = "NC";

                                        }
                                        else
                                        {
                                            doc = (SAPbobsCOM.Documents)Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInvoices);
                                            doc.DocumentSubType = SAPbobsCOM.BoDocumentSubType.bod_DebitMemo;
                                            typeDocumentCreated = "ND";
                                        }

                                        differentAmount = (differentAmount > 0 ? differentAmount : (differentAmount * -1));
                                        doc.CardCode = cardCode.Value;
                                        doc.DocDueDate = System.DateTime.Now;

                                        var comment = string.Empty;

                                        foreach (var item in OrdersToProcess)
                                        {
                                            if (string.IsNullOrEmpty(comment))
                                            {
                                                comment = item.ToString();
                                            }
                                            else
                                            {
                                                comment = comment + "," + item.ToString();
                                            }

                                        }

                                        doc.Comments = "Creado por Addon Transelect  para las ordenes - " + comment;
                                        doc.PointOfIssueCode = Globals.POIIP;
                                        doc.Letter = SAPbobsCOM.FolioLetterEnum.fLetterA;
                                        doc.Lines.ItemCode = Globals.ItemCodeAj;
                                        doc.Lines.Quantity = 1;
                                        doc.Indicator = "NL";
                                        doc.Lines.TaxCode = "IVA_0";
                                        doc.DocTotal = Convert.ToDouble(differentAmount.ToString());
                                        doc.Lines.Add();


                                        var resultToAdd = doc.Add();
                                        var error = Globals.SBOCompany.GetLastErrorCode() + " - " + Globals.SBOCompany.GetLastErrorDescription();
                                        var lastId = Globals.SBOCompany.GetNewObjectKey();


                                        if (resultToAdd == 0)
                                        {
                                            Globals.SBOApplication.StatusBar.SetText($"se creo la {typeDocument} nro {lastId} correctamente!", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
                                            bubbleEvent = false;
                                            documentCreated = Convert.ToInt32(lastId);
                                            generalRate = Convert.ToDecimal(amountGeneralRate);
                                            ReloadForm(cardCode.Value, Convert.ToInt32(lastId), typeDocument, Convert.ToDecimal(txtDiffRate.Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator)), out bubbleEvent);
                                        }
                                        else
                                        {
                                            Globals.SBOApplication.StatusBar.SetText($"hubo un error al intentar crear la {typeDocument} {error}!", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                                        }
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                Utils.LogError("Pagos_efectuados.Button1_ClickBefore" + Utils.GetLineException(ex) + ".Ex:", ex);
                            }

                            break;
                    }
                }
            }



        }

        private static void Cancel(ref String formUID, ref SAPbouiCOM.ItemEvent itemEvent, out bool bubbleEvent)
        {
            bubbleEvent = true;

            if (documentCreated != 0)
            {
                SAPbouiCOM.Form form;
                SAPbobsCOM.Documents doc;

                if (typeDocumentCreated == "NC")
                {
                    doc = (SAPbobsCOM.Documents)Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oCreditNotes);

                }
                else
                {
                    doc = (SAPbobsCOM.Documents)Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInvoices);
                }

                doc.GetByKey(documentCreated);
                SAPbobsCOM.Documents docCancel = doc.CreateCancellationDocument();
                docCancel.Add();

            }
        }
    }
}
