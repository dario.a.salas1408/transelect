﻿using SAPbouiCOM;
using SAPbouiCOM.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TranSelect.Model;

namespace TranSelect.SAPForms
{
    class fhIncPayment : SystemFormBase
    {
        private static decimal rate;
        private static decimal generalRate;
        //private static List<int> OrdersToProcess;
        private static List<ClsOrdersToProcess> OrdersToProcess;
        private static string formID;
        private Button Button1;
        private StaticText StaticText0;
        private EditText EditText0;
        private static bool reloadGrid = false;
        private StaticText StaticText1;
        private EditText EditText1;
        private static int documentCreated;
        private static string typeDocumentCreated;
        private static decimal differenceToApplique;
        private static string typeToApplique;
        private static string docNumPayment;

        public fhIncPayment()
        {
            this.iniComponent();

        }

        private void iniComponent()
        {
            //this.Button1 = ((SAPbouiCOM.Button)(this.GetItem("Item_1").Specific));
            //this.StaticText0 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_0").Specific));
            //this.EditText0 = ((SAPbouiCOM.EditText)(this.GetItem("Item_2").Specific));
            //this.StaticText1 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_3").Specific));
            //this.EditText1 = ((SAPbouiCOM.EditText)(this.GetItem("Item_4").Specific));
            //EditText0.DataBind.SetBound(true, "OVPM", "U_GeneralRate");
            //EditText1.DataBind.SetBound(true, "OVPM", "U_DifferenceRate");

            //SAPbobsCOM.SBObob sBObob = (SAPbobsCOM.SBObob)Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoBridge);

            //var rates = sBObob.GetCurrencyRate("USD", System.DateTime.Now);

            //while (!rates.EoF)
            //{
            //    rate = Convert.ToDecimal(rates.Fields.Item(0).Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator));
            //    rates.MoveNext();
            //}

            //this.EditText1.Item.Enabled = false;
        }

        public static void Event(string formUID, ref SAPbouiCOM.ItemEvent itemEvent, out bool bubbleEvent)
        {
            bubbleEvent = true;
            formID = formUID;
            try
            {
                switch (itemEvent.EventType)
                {
                    case SAPbouiCOM.BoEventTypes.et_RIGHT_CLICK:
                        //Load(ref formUID, ref itemEvent, out bubbleEvent);
                        var aaa = "";
                        break;
                    case SAPbouiCOM.BoEventTypes.et_FORM_LOAD:
                        Load(ref formUID, ref itemEvent, out bubbleEvent);
                        break;
                    case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS:
                        //Load(ref formUID, ref itemEvent, out bubbleEvent);

                        switch (itemEvent.ItemUID)
                        {
                            case "Item_2":
                                GrillaClick(ref formUID, ref itemEvent, out bubbleEvent);
                                break;
                        }

                        break;
                    case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST:
                        break;
                    case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED:
                        Pressed(ref formUID, ref itemEvent, out bubbleEvent);
                        break;
                    case SAPbouiCOM.BoEventTypes.et_CLICK:
                        switch (itemEvent.ItemUID)
                        {
                            case "20":
                                if (itemEvent.ColUID == "10000127")
                                {
                                    GrillaClick(ref formUID, ref itemEvent, out bubbleEvent);
                                }
                                break;
                            case "2":
                                Cancel(ref formUID, ref itemEvent, out bubbleEvent);
                                break;
                        }
                        break;
                    case SAPbouiCOM.BoEventTypes.et_VALIDATE:
                        break;
                }

            }
            catch (Exception ex)
            {
                Utils.LogError("Pagos_efectuados.Event.Line" + Utils.GetLineException(ex) + ".Ex:", ex);
            }
        }

        private static void Load(ref String formUID, ref SAPbouiCOM.ItemEvent itemEvent, out bool bubbleEvent)
        {
            bubbleEvent = true;
            SAPbouiCOM.Form oForm;
            SAPbouiCOM.Item oItem;
            SAPbouiCOM.StaticText oStatic;
            SAPbouiCOM.EditText oTextSec;
            SAPbouiCOM.Button btnCalculate;
            oForm = Globals.SBOApplication.Forms.Item(formUID);
            try
            {
                oForm.Freeze(true);
                SAPbouiCOM.ComboBox cmbCtrl;


                EditText docnum;
                docnum = (EditText)oForm.Items.Item("3").Specific;
                docNumPayment = docnum.Value.ToString();

                oItem = oForm.Items.Add("Item_1", SAPbouiCOM.BoFormItemTypes.it_BUTTON);
                btnCalculate = oItem.Specific as SAPbouiCOM.Button;
                oItem.Left = oForm.Items.Item("2").Left + 75;
                oItem.Width = oForm.Items.Item("2").Width + 60;
                oItem.Top = oForm.Items.Item("2").Top;
                oItem.Height = oForm.Items.Item("2").Height;
                oItem.Enabled = false;
                btnCalculate.Caption = "Calcular Monto a Pagar";

                oItem = oForm.Items.Add("Item_2", SAPbouiCOM.BoFormItemTypes.it_EDIT);
                oTextSec = oItem.Specific as SAPbouiCOM.EditText;
                oItem.Left = oForm.Items.Item("26").Left;
                oItem.Width = 80;
                oItem.Top = oForm.Items.Item("26").Top - 40;
                oTextSec.DataBind.SetBound(true, "ORCT", "U_GeneralRate");
                oItem = oForm.Items.Add("Item_0", SAPbouiCOM.BoFormItemTypes.it_STATIC);
                oStatic = oItem.Specific as SAPbouiCOM.StaticText;
                oItem.Left = oForm.Items.Item("27").Left;
                oItem.Width = 100;
                oItem.Top = oForm.Items.Item("27").Top - 40;
                oItem.LinkTo = "Item_2";
                oStatic.Caption = "Cotización General";

                oItem = oForm.Items.Add("Item_4", SAPbouiCOM.BoFormItemTypes.it_EDIT);
                oTextSec = oItem.Specific as SAPbouiCOM.EditText;
                oItem.Left = oForm.Items.Item("26").Left;
                oItem.Width = 80;
                oItem.Top = oForm.Items.Item("26").Top - 20;
                oItem.Enabled = false;
                oTextSec.DataBind.SetBound(true, "ORCT", "U_DifferenceRate");
                oItem = oForm.Items.Add("Item_3", SAPbouiCOM.BoFormItemTypes.it_STATIC);
                oStatic = oItem.Specific as SAPbouiCOM.StaticText;
                oItem.Left = oForm.Items.Item("27").Left;
                oItem.Width = 100;
                oItem.Top = oForm.Items.Item("27").Top - 20;
                oItem.LinkTo = "Item_4";
                oStatic.Caption = "Monto ajuste total";

                SAPbobsCOM.SBObob sBObob = (SAPbobsCOM.SBObob)Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoBridge);

                var rates = sBObob.GetCurrencyRate("USD", System.DateTime.Now);

                while (!rates.EoF)
                {
                    rate = Convert.ToDecimal(rates.Fields.Item(0).Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator));
                    rates.MoveNext();
                }

                OrdersToProcess = new List<ClsOrdersToProcess>();
                documentCreated = 0;

            }
            catch (Exception ex)
            {
                Utils.LogError("Event line " + Utils.GetLineException(ex) + " - Error:", ex);
            }
            finally
            {
                oForm.Freeze(false);
            }
        }

        private static void GrillaClick(ref String formUID, ref SAPbouiCOM.ItemEvent itemEvent, out bool bubbleEvent)
        {
            bubbleEvent = true;

            if (reloadGrid)
            {
                bubbleEvent = false;
                return;
            }

            Form form;
            Matrix matrix;
            form = Globals.SBOApplication.Forms.Item(formUID);
            matrix = (Matrix)form.Items.Item("20").Specific;
            form.Freeze(true);
            var bttnCalculate = (Button)form.Items.Item("Item_1").Specific;
            var cardCode = (EditText)form.Items.Item("5").Specific;
            decimal totalDoc = 0;
            decimal totalUSD = 0;
            string ls_sqlVal = string.Empty;
            decimal totalWhenGrlRate = 0;

            for (int i = 1; i <= matrix.RowCount; i++)
            {
                var typeDocVal = (EditText)matrix.Columns.Item("45").Cells.Item(i).Specific;
                var orderNumVal = (EditText)matrix.Columns.Item("1").Cells.Item(i).Specific;
                var selectedCheckBoxVal = (CheckBox)matrix.Columns.Item("10000127").Cells.Item(i).Specific;
                SAPbobsCOM.Recordset oRecordsetVal = null;
                oRecordsetVal = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                var udfDiff = (EditText)matrix.Columns.Item("U_DifferenceRate").Cells.Item(i).Specific;
                var udfDifRateOri = (EditText)matrix.Columns.Item("U_DiffRateOri").Cells.Item(i).Specific;
                var udfRateOri = (EditText)matrix.Columns.Item("U_RateOri").Cells.Item(i).Specific;
                var difRateTxt = (EditText)form.Items.Item("Item_4").Specific;
                udfDiff.Value = "0.00";
                udfDifRateOri.Value = "0.00";
                udfRateOri.Value = "0.00";
                EditText generalRateTxt;
                generalRateTxt = (EditText)form.Items.Item("Item_2").Specific;
                string amountGeneralRate = string.Empty;
                amountGeneralRate = generalRateTxt.Value.Replace(',', Globals.separator).Replace('.', Globals.separator);
                amountGeneralRate = (amountGeneralRate == "" ? "0,0".Replace(',', Globals.separator).Replace('.', Globals.separator) : amountGeneralRate);
                EditText amountTxtDUE;
                EditText amountTxtHolding;
                string amountDUE = string.Empty;
                string amountHolding = string.Empty;
                amountTxtDUE = (EditText)form.Items.Item("45").Specific;
                amountTxtHolding = (EditText)form.Items.Item("253000195").Specific;
                decimal amountToOP;



                if (amountTxtDUE.Value != "")
                {
                    if (amountTxtDUE.Value.Length > 0)
                    {
                        amountDUE = amountTxtDUE.Value.Substring(0, amountTxtDUE.Value.Length - 3);
                        amountDUE = amountDUE.Replace(',', Globals.separator).Replace('.', Globals.separator);
                    }
                    else
                    {
                        amountDUE = "0";
                    }

                    if (amountTxtHolding.Value.Length > 0)
                    {
                        amountHolding = amountTxtHolding.Value.Substring(0, amountTxtHolding.Value.Length - 3);
                        amountHolding = amountHolding.Replace(',', Globals.separator).Replace('.', Globals.separator);
                    }
                    else
                    {
                        amountHolding = "0";
                    }
                }
                else
                {
                    amountHolding = "0";
                    amountDUE = "0";
                }

                amountToOP = Convert.ToDecimal(amountDUE) + Convert.ToDecimal(amountHolding);

                if (selectedCheckBoxVal.Checked == true)
                {
                    if (typeDocVal.Value == "13")
                    {
                        ls_sqlVal = string.Format(Globals.DbQuery.GetString("IncPay01"), orderNumVal.Value, cardCode.Value);
                    }
                    else
                    {
                        if (typeDocVal.Value == "14")
                        {
                            ls_sqlVal = string.Format(Globals.DbQuery.GetString("IncPay02"), orderNumVal.Value, cardCode.Value);
                        }
                        else
                        {
                            ls_sqlVal = string.Format(Globals.DbQuery.GetString("IncPay06"), orderNumVal.Value, cardCode.Value);
                        }
                    }

                    oRecordsetVal.DoQuery(ls_sqlVal);

                    if (!oRecordsetVal.EoF)
                    {
                        OrdersToProcess.Add(new ClsOrdersToProcess { DocEntry = Convert.ToInt32(oRecordsetVal.Fields.Item("DocEntry").Value.ToString()), OrderId = Convert.ToInt32(orderNumVal.Value), Type = typeDocVal.Value, MontUSD = Convert.ToDecimal(oRecordsetVal.Fields.Item("DocTotalFC").Value.ToString()), InitMont = Convert.ToDecimal(oRecordsetVal.Fields.Item("DocTotal").Value.ToString()), InitRate = Convert.ToDecimal(oRecordsetVal.Fields.Item("DocRate").Value.ToString()), CardCode = cardCode.Value });

                        while (!oRecordsetVal.EoF)
                        {
                            if (oRecordsetVal.Fields.Item("DocCur").Value.ToString() == "USD" || oRecordsetVal.Fields.Item("DocCur").Value.ToString() == "EUR" || oRecordsetVal.Fields.Item("DocCur").Value.ToString() == "USX")
                            {
                                if (oRecordsetVal.Fields.Item("DocCur").Value.ToString() != "USD")
                                {
                                    SAPbobsCOM.SBObob sBObob = (SAPbobsCOM.SBObob)Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoBridge);

                                    var rates = sBObob.GetCurrencyRate(oRecordsetVal.Fields.Item("DocCur").Value.ToString(), System.DateTime.Now);

                                    while (!rates.EoF)
                                    {
                                        rate = Convert.ToDecimal(rates.Fields.Item(0).Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator));
                                        rates.MoveNext();
                                    }
                                }

                                decimal totalLine = 0;

                                decimal totalLineWithCurrentRate = 0;

                                decimal rateToApply = rate;

                                decimal rateToApplyLine = Convert.ToDecimal(oRecordsetVal.Fields.Item("DocRate").Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator));
                                //udfRateOri.Value = oRecordsetVal.Fields.Item("DocRate").Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator);
                                udfRateOri.Value = oRecordsetVal.Fields.Item("DocRate").Value.ToString().Replace(',', '.');


                                var calc = Convert.ToDecimal(oRecordsetVal.Fields.Item("DocTotalFC").Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator));
                                udfDifRateOri.Value = (calc * rateToApplyLine).ToString().Replace(",", ".");

                                if (amountGeneralRate != ("0,0".Replace(',', Globals.separator).Replace('.', Globals.separator)))
                                {
                                    rateToApply = Convert.ToDecimal(amountGeneralRate);

                                }



                                totalLine = (Convert.ToDecimal(oRecordsetVal.Fields.Item("DocTotalFC").Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator)) - Convert.ToDecimal(oRecordsetVal.Fields.Item("PaidFC").Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator))) * rateToApplyLine;

                                totalLine = typeDocVal.Value == "13" ? (totalLine * 1) : (totalLine * -1);

                                totalDoc = totalDoc + totalLine;

                                totalUSD = totalUSD + (Convert.ToDecimal(oRecordsetVal.Fields.Item("DocTotalFC").Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator)) - Convert.ToDecimal(oRecordsetVal.Fields.Item("PaidFC").Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator)));

                                totalLineWithCurrentRate = (Convert.ToDecimal(oRecordsetVal.Fields.Item("DocTotalFC").Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator)) - Convert.ToDecimal(oRecordsetVal.Fields.Item("PaidFC").Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator))) * rateToApply;

                                totalLineWithCurrentRate = typeDocVal.Value == "13" ? (totalLineWithCurrentRate * 1) : (totalLineWithCurrentRate * -1);

                                totalLine = totalLine - totalLineWithCurrentRate;

                                udfDiff.Value = totalLine.ToString().Replace(",", ".");
                            }
                            else
                            {
                                string ls_sql = string.Empty;

                                decimal totalLineWithCurrentRate = 0;

                                if (typeDocVal.Value == "13")
                                {
                                    ls_sql = string.Format(Globals.DbQuery.GetString("IncPay01"), orderNumVal.Value, cardCode.Value);
                                }
                                else
                                {
                                    if (typeDocVal.Value == "14")
                                    {
                                        ls_sqlVal = string.Format(Globals.DbQuery.GetString("IncPay02"), orderNumVal.Value, cardCode.Value);
                                    }
                                    else
                                    {
                                        ls_sqlVal = string.Format(Globals.DbQuery.GetString("IncPay06"), orderNumVal.Value, cardCode.Value);
                                    }
                                }

                                SAPbobsCOM.Recordset oRecordset = null;
                                oRecordset = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;

                                oRecordsetVal.DoQuery(ls_sqlVal);

                                totalLineWithCurrentRate = typeDocVal.Value == "13" ? (Convert.ToDecimal(oRecordsetVal.Fields.Item("DocTotal").Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator)) * 1) : (Convert.ToDecimal(oRecordsetVal.Fields.Item("DocTotal").Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator)) * -1);

                                totalDoc = totalDoc + totalLineWithCurrentRate;

                                totalUSD = totalUSD + (Convert.ToDecimal(oRecordsetVal.Fields.Item("DocTotalFC").Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator)) - Convert.ToDecimal(oRecordsetVal.Fields.Item("PaidFC").Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator)));

                            }

                            oRecordsetVal.MoveNext();
                        }

                    }

                }
                else
                {
                    var objToDelete = OrdersToProcess.Where(c => c.Type == typeDocVal.Value && c.OrderId == Convert.ToInt32(orderNumVal.Value)).FirstOrDefault();
                    OrdersToProcess.Remove(objToDelete);
                }

                totalWhenGrlRate = totalWhenGrlRate + Convert.ToDecimal(udfDiff.Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator));

                if (amountGeneralRate == ("0,0".Replace(',', Globals.separator).Replace('.', Globals.separator)))
                {
                    difRateTxt.Value = (totalDoc - amountToOP).ToString().Replace(",", ".");
                }
                else
                {
                    difRateTxt.Value = (totalDoc - amountToOP - totalWhenGrlRate).ToString().Replace(",", ".");
                }


                if ((totalDoc - amountToOP) != 0)
                {
                    bttnCalculate.Item.Enabled = true;
                }
                else
                {
                    bttnCalculate.Item.Enabled = false;
                }

                //if (amountGeneralRate == ("0,0".Replace(',', Globals.separator).Replace('.', Globals.separator)))
                //{
                //    difRateTxt.Value = (totalDoc - amountToOP).ToString().Replace(",", ".");

                //    if ((totalDoc - amountToOP) != 0)
                //    {
                //        bttnCalculate.Item.Enabled = true;
                //    }
                //    else
                //    {
                //        bttnCalculate.Item.Enabled = false;
                //    }
                //}
                //else
                //{

                //    if (rate != Convert.ToDecimal(amountGeneralRate))
                //    {
                //        decimal totalPesos = totalUSD * Convert.ToDecimal(amountGeneralRate);

                //        difRateTxt.Value = (totalPesos - amountToOP).ToString().Replace(",", ".");


                //        if ((totalPesos - amountToOP) != 0)
                //        {
                //            bttnCalculate.Item.Enabled = true;
                //        }
                //        else
                //        {
                //            bttnCalculate.Item.Enabled = false;
                //        }
                //    }
                //    else
                //    {
                //        difRateTxt.Value = (totalDoc - amountToOP).ToString().Replace(",", ".");

                //        if ((totalDoc - amountToOP) != 0)
                //        {
                //            bttnCalculate.Item.Enabled = true;
                //        }
                //        else
                //        {
                //            bttnCalculate.Item.Enabled = false;
                //        }
                //    }

                //}


            }

            form.Freeze(false);

        }

        private static void ReloadForm(string cardCode, int orderAj, string typeDocument, decimal totalAdj, out bool BubbleEvent)
        {
            BubbleEvent = false;
            reloadGrid = true;

            Form form;
            OptionBtn opVendor;
            OptionBtn opCustomer;
            EditText txtCardCode;
            Matrix matrix;
            Button bttnCalculate;

            int type = 13;
            try
            {
                form = Globals.SBOApplication.Forms.Item(formID);
                form.Freeze(true);
                opCustomer = (OptionBtn)form.Items.Item("56").Specific;
                opVendor = (OptionBtn)form.Items.Item("57").Specific;
                txtCardCode = (EditText)form.Items.Item("5").Specific;
                matrix = (Matrix)form.Items.Item("20").Specific;
                bttnCalculate = (Button)form.Items.Item("Item_1").Specific;
                type = (typeDocument == "Nota de Credito" ? 14 : 13);

                opCustomer.Item.Click();
                opVendor.Item.Click();
                opCustomer.Item.Click();
                txtCardCode.Value = cardCode;

                var difRateTxt = (EditText)form.Items.Item("Item_4").Specific;
                decimal diff = Convert.ToDecimal(difRateTxt.Value.Replace(',', Globals.separator).Replace('.', Globals.separator));
                EditText generalRateTxt;
                generalRateTxt = (EditText)form.Items.Item("Item_2").Specific;

                string ls_sqlVal = string.Empty;
                SAPbobsCOM.Recordset oRecordsetVal = null;
                oRecordsetVal = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;

                foreach (var item in OrdersToProcess.Distinct().ToList())
                {
                    for (int i = 1; i <= matrix.RowCount; i++)
                    {
                        var orderNum = (EditText)matrix.Columns.Item("1").Cells.Item(i).Specific;
                        var typeDoc = (EditText)matrix.Columns.Item("45").Cells.Item(i).Specific;
                        var udfDiff = (EditText)matrix.Columns.Item("U_DifferenceRate").Cells.Item(i).Specific;

                        if (Convert.ToInt32(orderNum.Value) == item.OrderId)
                        {
                            var selectedCheckBox = (CheckBox)matrix.Columns.Item("10000127").Cells.Item(i).Specific;
                            selectedCheckBox.Checked = true;

                            if (typeDoc.Value == "13")
                            {
                                ls_sqlVal = string.Format(Globals.DbQuery.GetString("IncPay01"), item.OrderId, cardCode);
                            }
                            else
                            {
                                if (typeDoc.Value == "14")
                                {
                                    ls_sqlVal = string.Format(Globals.DbQuery.GetString("IncPay02"), item.OrderId, cardCode);
                                }
                                else
                                {
                                    ls_sqlVal = string.Format(Globals.DbQuery.GetString("IncPay06"), item.OrderId, cardCode);
                                }
                            }

                            oRecordsetVal.DoQuery(ls_sqlVal);

                            if (!oRecordsetVal.EoF)
                            {

                                while (!oRecordsetVal.EoF)
                                {
                                    if (oRecordsetVal.Fields.Item("DocCur").Value.ToString() == "USD" || oRecordsetVal.Fields.Item("DocCur").Value.ToString() == "EUR" || oRecordsetVal.Fields.Item("DocCur").Value.ToString() == "USX")
                                    {

                                        decimal totalLine = 0;

                                        decimal totalLineWithCurrentRate = 0;

                                        decimal rateToApply = rate;

                                        decimal rateToApplyLine = Convert.ToDecimal(oRecordsetVal.Fields.Item("DocRate").Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator));

                                        if (generalRate != 0)
                                        {
                                            rateToApplyLine = Convert.ToDecimal(generalRate);
                                        }

                                        totalLine = Convert.ToDecimal(oRecordsetVal.Fields.Item("DocTotalFC").Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator)) * rateToApplyLine;

                                        totalLine = typeDoc.Value == "13" ? (totalLine * 1) : (totalLine * -1);

                                        totalLineWithCurrentRate = Convert.ToDecimal(oRecordsetVal.Fields.Item("DocTotalFC").Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator)) * rateToApply;

                                        totalLineWithCurrentRate = typeDoc.Value == "13" ? (totalLineWithCurrentRate * 1) : (totalLineWithCurrentRate * -1);

                                        totalLine = totalLine - totalLineWithCurrentRate;

                                        udfDiff.Value = totalLine.ToString().Replace(",", ".");
                                    }


                                    oRecordsetVal.MoveNext();
                                }

                            }
                        }
                    }
                }

                difRateTxt.Item.Enabled = true;
                difRateTxt.Value = totalAdj.ToString().Replace(",", ".");
                generalRateTxt.Value = generalRate.ToString().Replace(",", ".");
                difRateTxt.Item.Enabled = false;

                SAPbobsCOM.Recordset oRecordsetGetDocNum = null;
                oRecordsetGetDocNum = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                String ls_sqlDocNum = string.Empty;

                if (type == 13)
                {
                    ls_sqlDocNum = string.Format(Globals.DbQuery.GetString("IncPay03"), orderAj, cardCode);
                }
                else
                {
                    ls_sqlDocNum = string.Format(Globals.DbQuery.GetString("IncPay04"), orderAj, cardCode);
                }

                oRecordsetGetDocNum.DoQuery(ls_sqlDocNum);

                while (!oRecordsetGetDocNum.EoF)
                {
                    orderAj = Convert.ToInt32(oRecordsetGetDocNum.Fields.Item("DocNum").Value);
                    oRecordsetGetDocNum.MoveNext();
                }

                for (int i = 1; i <= matrix.RowCount; i++)
                {
                    var orderNum = (EditText)matrix.Columns.Item("1").Cells.Item(i).Specific;
                    var typeDoc = (EditText)matrix.Columns.Item("45").Cells.Item(i).Specific;

                    if (Convert.ToInt32(orderNum.Value) == orderAj)
                    {
                        if (typeDoc.Value == "13")
                        {
                            SAPbobsCOM.Recordset oRecordset = null;
                            oRecordset = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                            String ls_sql = string.Format(Globals.DbQuery.GetString("IncPay05"), orderNum.Value, cardCode);
                            oRecordset.DoQuery(ls_sql);

                            if (!oRecordset.EoF)
                            {
                                var selectedCheckBox = (CheckBox)matrix.Columns.Item("10000127").Cells.Item(i).Specific;
                                selectedCheckBox.Checked = true;
                            }

                        }
                        else
                        {
                            var selectedCheckBox = (CheckBox)matrix.Columns.Item("10000127").Cells.Item(i).Specific;
                            selectedCheckBox.Checked = true;
                        }

                    }
                }
                reloadGrid = false;
                //OrdersToProcess.Clear();
                bttnCalculate.Item.Enabled = false;
                form.Freeze(false);

            }
            catch (Exception ex)
            {
                Utils.LogError("Pagos_efectuados.ReloadForm" + Utils.GetLineException(ex) + ".Ex:", ex);
            }
        }

        private static void Pressed(ref String formUID, ref SAPbouiCOM.ItemEvent itemEvent, out bool bubbleEvent)
        {
            bubbleEvent = true;
            Form form;

            if (itemEvent.BeforeAction == false)
            {
                if (itemEvent.ActionSuccess == true)
                {
                    switch (itemEvent.ItemUID)
                    {
                        case "Item_1":

                            try
                            {

                                EditText cardCode;
                                EditText generalRateTxt;
                                string amountGeneralRate = string.Empty;
                                form = Globals.SBOApplication.Forms.Item(formID);
                                cardCode = (EditText)form.Items.Item("5").Specific;
                                generalRateTxt = (EditText)form.Items.Item("Item_2").Specific;

                                amountGeneralRate = generalRateTxt.Value.Replace(',', Globals.separator).Replace('.', Globals.separator);
                                amountGeneralRate = (amountGeneralRate == "" ? "0,0".Replace(',', Globals.separator).Replace('.', Globals.separator) : amountGeneralRate);

                                decimal differentAmount = 0;

                                var txtDiffRate = (EditText)form.Items.Item("Item_4").Specific;

                                differentAmount = Convert.ToDecimal(txtDiffRate.Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator));
                                //differentAmount = Convert.ToDecimal(txtDiffRate.Value.ToString());

                                if (differentAmount != 0)
                                {
                                    var typeDocument = (differentAmount > 0 ? "Nota de Debito" : "Nota de Credito");



                                    if (Globals.SBOApplication.MessageBox($"Hay una diferencia por el tipo de cambio en los documentos de {differentAmount.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator)}, se generará una {typeDocument} por este monto, para resolver este desajuste, desea realizarlo?", 2, "Yes", "No", "") == 1)
                                    {
                                        SAPbobsCOM.Documents doc;
                                        SAPbobsCOM.Documents docNew;

                                        if (typeDocument == "Nota de Credito")
                                        {
                                            doc = (SAPbobsCOM.Documents)Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oCreditNotes);
                                            typeDocumentCreated = "NC";
                                            typeToApplique = "NC";
                                        }
                                        else
                                        {
                                            doc = (SAPbobsCOM.Documents)Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInvoices);
                                            doc.DocumentSubType = SAPbobsCOM.BoDocumentSubType.bod_DebitMemo;
                                            typeDocumentCreated = "ND";
                                            typeToApplique = "ND";
                                        }

                                        differentAmount = (differentAmount > 0 ? differentAmount : (differentAmount * -1));
                                        doc.CardCode = cardCode.Value;
                                        doc.DocDueDate = System.DateTime.Now;

                                        differenceToApplique = differentAmount;

                                        //var comment = string.Empty;

                                        //foreach (var item in OrdersToProcess)
                                        //{
                                        //    if (string.IsNullOrEmpty(comment))
                                        //    {
                                        //        comment = item.OrderId.ToString();
                                        //    }
                                        //    else
                                        //    {
                                        //        comment = comment + "," + item.OrderId.ToString();
                                        //    }

                                        //}


                                        doc.Comments = "Creado por Addon Transelect ";
                                        doc.DiscountPercent = 0;
                                        doc.PointOfIssueCode = Globals.POIIP;
                                        doc.Letter = SAPbobsCOM.FolioLetterEnum.fLetterA;
                                        doc.ImportFileNum = 1;
                                        doc.Lines.ItemCode = Globals.ItemCodeAj;
                                        doc.Lines.Price = Convert.ToDouble(differentAmount.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator)) / 1.21;
                                        doc.Lines.Quantity = 1;
                                        doc.Indicator = "NL";
                                        doc.Lines.TaxCode = "IVA_21";
                                        doc.Rounding = SAPbobsCOM.BoYesNoEnum.tYES;
                                        doc.Lines.Add();

                                        var resultToAdd = doc.Add();
                                        var error = Globals.SBOCompany.GetLastErrorCode() + " - " + Globals.SBOCompany.GetLastErrorDescription();
                                        var lastId = Globals.SBOCompany.GetNewObjectKey();

                                        if (resultToAdd != 0)
                                        {
                                            Globals.SBOApplication.StatusBar.SetText($"hubo un error al intentar crear la {typeDocument} {error}!", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                                            return;
                                        }

                                        doc.GetByKey(Convert.ToInt32(lastId));

                                        if (doc.WTAmount > 0)
                                        {
                                            if (typeDocument == "Nota de Credito")
                                            {
                                                docNew = (SAPbobsCOM.Documents)Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oCreditNotes);
                                                typeDocumentCreated = "NC";

                                            }
                                            else
                                            {
                                                docNew = (SAPbobsCOM.Documents)Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInvoices);
                                                docNew.DocumentSubType = SAPbobsCOM.BoDocumentSubType.bod_DebitMemo;
                                                typeDocumentCreated = "ND";
                                            }

                                            docNew.CardCode = cardCode.Value;
                                            doc.DiscountPercent = 0;
                                            docNew.DocDueDate = System.DateTime.Now;
                                            docNew.Rounding = SAPbobsCOM.BoYesNoEnum.tYES;
                                            docNew.RoundingDiffAmount = doc.WTAmount * -1;
                                            docNew.Comments = "Creado por Addon Transelect";
                                            docNew.PointOfIssueCode = Globals.POIIP;
                                            docNew.Letter = SAPbobsCOM.FolioLetterEnum.fLetterA;
                                            docNew.Lines.ItemCode = Globals.ItemCodeAj;
                                            docNew.Lines.Price = Convert.ToDouble(differentAmount.ToString()) / 1.21;
                                            docNew.Lines.Quantity = 1;
                                            docNew.Indicator = "NL";
                                            docNew.Lines.TaxCode = "IVA_21";
                                            docNew.Lines.Add();

                                            SAPbobsCOM.Documents docCancel = doc.CreateCancellationDocument();
                                            docCancel.Add();

                                            resultToAdd = docNew.Add();
                                            error = Globals.SBOCompany.GetLastErrorCode() + " - " + Globals.SBOCompany.GetLastErrorDescription();
                                            lastId = Globals.SBOCompany.GetNewObjectKey();

                                        }

                                        if (resultToAdd == 0)
                                        {
                                            Globals.SBOApplication.StatusBar.SetText($"se creo la {typeDocument} nro {lastId} correctamente!", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
                                            bubbleEvent = false;
                                            documentCreated = Convert.ToInt32(lastId);
                                            generalRate = Convert.ToDecimal(amountGeneralRate);
                                            ReloadForm(cardCode.Value, Convert.ToInt32(lastId), typeDocument, Convert.ToDecimal(txtDiffRate.Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator)), out bubbleEvent);
                                        }
                                        else
                                        {
                                            Globals.SBOApplication.StatusBar.SetText($"hubo un error al intentar crear la {typeDocument} {error}!", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error);
                                        }
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                Utils.LogError("Pagos_efectuados.Button1_ClickBefore" + Utils.GetLineException(ex) + ".Ex:", ex);
                            }

                            break;
                        case "1":
                            SAPbobsCOM.Recordset oRecordsetVal = null;
                            oRecordsetVal = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                            SAPbobsCOM.Recordset oRecordsetLog = null;
                            oRecordsetLog = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                            var table = string.Empty;
                            var table16 = string.Empty;

                            if (documentCreated > 0)
                            {
                                foreach (var item in OrdersToProcess)
                                {
                                    if (item.Type != "24")
                                    {
                                        switch (item.Type)
                                        {
                                            case "13":
                                                table = "OINV";
                                                table16 = "INV6";
                                                break;
                                            case "14":
                                                table = "ORIN";
                                                table16 = "RIN6";
                                                break;
                                        }

                                        //var table = item.Type == "13" ? "OINV" : "ORIN";
                                        var ls_sql = string.Format(Globals.DbQuery.GetString("changeR"), table, item.InitRate.ToString().Replace(',', '.'), item.DocEntry, item.CardCode);
                                        //Globals.SBOApplication.StatusBar.SetText(ls_sql, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                        oRecordsetVal.DoQuery(ls_sql);


                                        //Change 05-------------------------------------------------------------------------------------------------------------------------------------------
                                        ls_sql = string.Format(Globals.DbQuery.GetString("change05"), (item.InitRate * item.MontUSD).ToString().Replace(',', '.'), item.DocEntry, item.Type);
                                        var ls_sqlLog = string.Format(Globals.DbQuery.GetString("Log05"), (item.InitRate * item.MontUSD).ToString().Replace(',', '.'), item.DocEntry, item.Type);

                                        oRecordsetLog.DoQuery(ls_sqlLog);
                                        while (!oRecordsetLog.EoF)
                                        {


                                            Utils.LogChanges(new clsLogChange { UpdatedDo = ls_sql, TableUpdated = "JDT1", LineCode = "858", PaymentCode = item.DocEntry.ToString(), PaymentType = item.Type, Debit = Convert.ToDecimal(oRecordsetLog.Fields.Item("Debit").Value) });
                                            oRecordsetLog.MoveNext();
                                        }

                                        //Run Update 05
                                        oRecordsetVal.DoQuery(ls_sql);
                                        //-----------------------------------------------------------------------------------------------------------------------------------------------

                                        //Change 09-------------------------------------------------------------------------------------------------------------------------------------------
                                        ls_sql = string.Format(Globals.DbQuery.GetString("change09"), docNumPayment, "24");
                                        ls_sqlLog = string.Format(Globals.DbQuery.GetString("Log09"), docNumPayment, "24");
                                        oRecordsetLog.DoQuery(ls_sqlLog);
                                        while (!oRecordsetLog.EoF)
                                        {


                                            Utils.LogChanges(new clsLogChange { UpdatedDo = ls_sql, TableUpdated = "JDT1", LineCode = "874", PaymentCode = docNumPayment, PaymentType = "24", SYSCred = Convert.ToDecimal(oRecordsetLog.Fields.Item("SYSCred").Value) });
                                            oRecordsetLog.MoveNext();
                                        }

                                        //Run Update 09
                                        oRecordsetVal.DoQuery(ls_sql);
                                        //-----------------------------------------------------------------------------------------------------------------------------------------------


                                        //Change 10-------------------------------------------------------------------------------------------------------------------------------------------
                                        ls_sql = string.Format(Globals.DbQuery.GetString("change10"), docNumPayment, "24");
                                        ls_sqlLog = string.Format(Globals.DbQuery.GetString("Log10"), docNumPayment, "24");
                                        oRecordsetLog.DoQuery(ls_sqlLog);
                                        while (!oRecordsetLog.EoF)
                                        {


                                            Utils.LogChanges(new clsLogChange { UpdatedDo = ls_sql, TableUpdated = "JDT1", LineCode = "890", PaymentCode = docNumPayment, PaymentType = "24", SYSCred = Convert.ToDecimal(oRecordsetLog.Fields.Item("SYSCred").Value) });
                                            oRecordsetLog.MoveNext();
                                        }


                                        //Run Update 10
                                        oRecordsetVal.DoQuery(ls_sql);
                                        //-----------------------------------------------------------------------------------------------------------------------------------------------



                                    }

                                }
                            }



                            OrdersToProcess.Clear();

                            break;

                    }

                }
                else
                {
                    switch (itemEvent.ItemUID)
                    {
                        case "1":

                            SAPbobsCOM.Recordset oRecordsetVal = null;
                            oRecordsetVal = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                            SAPbobsCOM.Recordset oRecordsetLog = null;
                            oRecordsetLog = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                            var table = string.Empty;
                            var table16 = string.Empty;

                            if (documentCreated > 0)
                            {
                                foreach (var item in OrdersToProcess)
                                {
                                    if (item.Type != "24")
                                    {
                                        switch (item.Type)
                                        {
                                            case "13":
                                                table = "OINV";
                                                table16 = "INV6";
                                                break;
                                            case "14":
                                                table = "ORIN";
                                                table16 = "RIN6";
                                                break;
                                        }

                                        var ls_sql = string.Format(Globals.DbQuery.GetString("changeR"), table, (item.InitRate).ToString().Replace(',', '.'), item.DocEntry, item.CardCode);
                                        //Globals.SBOApplication.StatusBar.SetText(ls_sql, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                        oRecordsetVal.DoQuery(ls_sql);


                                        //Change 05-------------------------------------------------------------------------------------------------------------------------------------------
                                        ls_sql = string.Format(Globals.DbQuery.GetString("change05"), (rate * item.MontUSD).ToString().Replace(',', '.'), item.DocEntry, item.Type);
                                        var ls_sqlLog = string.Format(Globals.DbQuery.GetString("Log05"), (item.InitRate * item.MontUSD).ToString().Replace(',', '.'), item.DocEntry, item.Type);

                                        oRecordsetLog.DoQuery(ls_sqlLog);
                                        while (!oRecordsetLog.EoF)
                                        {


                                            Utils.LogChanges(new clsLogChange { UpdatedDo = ls_sql, TableUpdated = "JDT1", LineCode = "958", PaymentCode = item.DocEntry.ToString(), PaymentType = item.Type, Debit = Convert.ToDecimal(oRecordsetLog.Fields.Item("Debit").Value) });
                                            oRecordsetLog.MoveNext();
                                        }

                                        //Run Update 05
                                        oRecordsetVal.DoQuery(ls_sql);
                                        //-----------------------------------------------------------------------------------------------------------------------------------------------


                                        if (typeToApplique == "NC")
                                        {
                                            //Change 06-------------------------------------------------------------------------------------------------------------------------------------------
                                            ls_sql = string.Format(Globals.DbQuery.GetString("change06"), differenceToApplique.ToString().Replace(',', '.'), item.CardCode);
                                            ls_sqlLog = string.Format(Globals.DbQuery.GetString("Log0607"), differenceToApplique.ToString().Replace(',', '.'), item.CardCode);

                                            oRecordsetLog.DoQuery(ls_sqlLog);
                                            while (!oRecordsetLog.EoF)
                                            {


                                                Utils.LogChanges(new clsLogChange { UpdatedDo = ls_sql, TableUpdated = "OCRD", LineCode = "977", PaymentCode = item.CardCode, PaymentType = "", Balance = Convert.ToDecimal(oRecordsetLog.Fields.Item("Balance").Value) });
                                                oRecordsetLog.MoveNext();
                                            }

                                            //Run Update 06
                                            oRecordsetVal.DoQuery(ls_sql);
                                            //-----------------------------------------------------------------------------------------------------------------------------------------------
                                        }
                                        else
                                        {

                                            //Change 07-------------------------------------------------------------------------------------------------------------------------------------------
                                            ls_sql = string.Format(Globals.DbQuery.GetString("change07"), differenceToApplique.ToString().Replace(',', '.'), item.CardCode);
                                            ls_sqlLog = string.Format(Globals.DbQuery.GetString("Log0607"), differenceToApplique.ToString().Replace(',', '.'), item.CardCode);

                                            oRecordsetLog.DoQuery(ls_sqlLog);
                                            while (!oRecordsetLog.EoF)
                                            {


                                                Utils.LogChanges(new clsLogChange { UpdatedDo = ls_sql, TableUpdated = "OCRD", LineCode = "996", PaymentCode = item.CardCode, PaymentType = "", Balance = Convert.ToDecimal(oRecordsetLog.Fields.Item("Balance").Value) });
                                                oRecordsetLog.MoveNext();
                                            }

                                            //Run Update 07
                                            oRecordsetVal.DoQuery(ls_sql);
                                            //-----------------------------------------------------------------------------------------------------------------------------------------------

                                        }


                                    }
                                }
                            }

                            break;
                    }
                }
            }
            else
            {
                switch (itemEvent.ItemUID)
                {
                    case "1":

                        SAPbobsCOM.Recordset oRecordsetVal = null;
                        oRecordsetVal = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                        SAPbobsCOM.Recordset oRecordsetLog = null;
                        oRecordsetLog = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
                        var table = string.Empty;
                        var table16 = string.Empty;

                        if (documentCreated > 0)
                        {
                            foreach (var item in OrdersToProcess)
                            {
                                if (item.Type != "24")
                                {
                                    switch (item.Type)
                                    {
                                        case "13":
                                            table = "OINV";
                                            table16 = "INV6";
                                            break;
                                        case "14":
                                            table = "ORIN";
                                            table16 = "RIN6";
                                            break;
                                    }



                                    var ls_sql = string.Format(Globals.DbQuery.GetString("changeR"), table, rate.ToString().Replace(',', '.'), item.DocEntry, item.CardCode);
                                    //Globals.SBOApplication.StatusBar.SetText(ls_sql, BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Warning);
                                    oRecordsetVal.DoQuery(ls_sql);

                                    //Change 02-------------------------------------------------------------------------------------------------------------------------------------------
                                    ls_sql = string.Format(Globals.DbQuery.GetString("change02"), table16, (rate * item.MontUSD).ToString().Replace(',', '.'), item.DocEntry);
                                    var ls_sqlLog = string.Format(Globals.DbQuery.GetString("Log02"), table16, (rate * item.MontUSD).ToString().Replace(',', '.'), item.DocEntry);

                                    oRecordsetLog.DoQuery(ls_sqlLog);
                                    while (!oRecordsetLog.EoF)
                                    {

                                        Utils.LogChanges(new clsLogChange { UpdatedDo = ls_sql, TableUpdated = table16, LineCode = "1060", PaymentCode = item.DocEntry.ToString(), PaymentType = item.Type, InsTotal = Convert.ToDecimal(oRecordsetLog.Fields.Item("InsTotal").Value) });
                                        oRecordsetLog.MoveNext();
                                    }

                                    //Run Update 02
                                    oRecordsetVal.DoQuery(ls_sql);
                                    //-----------------------------------------------------------------------------------------------------------------------------------------------


                                    //Change 03-------------------------------------------------------------------------------------------------------------------------------------------
                                    ls_sql = string.Format(Globals.DbQuery.GetString("change03"), (rate * item.MontUSD).ToString().Replace(',', '.'), item.DocEntry, item.Type);
                                    ls_sqlLog = string.Format(Globals.DbQuery.GetString("Log03"), (rate * item.MontUSD).ToString().Replace(',', '.'), item.DocEntry, item.Type);

                                    oRecordsetLog.DoQuery(ls_sqlLog);
                                    while (!oRecordsetLog.EoF)
                                    {


                                        Utils.LogChanges(new clsLogChange { UpdatedDo = ls_sql, TableUpdated = "JDT1", LineCode = "1076", PaymentCode = item.DocEntry.ToString(), PaymentType = item.Type, BalDueDeb = Convert.ToDecimal(oRecordsetLog.Fields.Item("BalDueDeb").Value) });
                                        oRecordsetLog.MoveNext();
                                    }

                                    //Run Update 03
                                    oRecordsetVal.DoQuery(ls_sql);
                                    //-----------------------------------------------------------------------------------------------------------------------------------------------

                                    //Change 04-------------------------------------------------------------------------------------------------------------------------------------------
                                    ls_sql = string.Format(Globals.DbQuery.GetString("change04"), (rate * item.MontUSD).ToString().Replace(',', '.'), item.DocEntry, item.Type);
                                    ls_sqlLog = string.Format(Globals.DbQuery.GetString("Log04"), (rate * item.MontUSD).ToString().Replace(',', '.'), item.DocEntry, item.Type);

                                    oRecordsetLog.DoQuery(ls_sqlLog);
                                    while (!oRecordsetLog.EoF)
                                    {


                                        Utils.LogChanges(new clsLogChange { UpdatedDo = ls_sql, TableUpdated = "ITR1", LineCode = "1093", PaymentCode = item.DocEntry.ToString(), PaymentType = item.Type, ReconSum = Convert.ToDecimal(oRecordsetLog.Fields.Item("ReconSum").Value) });
                                        oRecordsetLog.MoveNext();
                                    }

                                    //Run Update 04
                                    oRecordsetVal.DoQuery(ls_sql);
                                    //-----------------------------------------------------------------------------------------------------------------------------------------------


                                    if (typeToApplique == "NC")
                                    {
                                        //Change 07-------------------------------------------------------------------------------------------------------------------------------------------
                                        ls_sql = string.Format(Globals.DbQuery.GetString("change07"), differenceToApplique.ToString().Replace(',', '.'), item.CardCode);
                                        ls_sqlLog = string.Format(Globals.DbQuery.GetString("Log0607"), differenceToApplique.ToString().Replace(',', '.'), item.CardCode);

                                        oRecordsetLog.DoQuery(ls_sqlLog);
                                        while (!oRecordsetLog.EoF)
                                        {


                                            Utils.LogChanges(new clsLogChange { UpdatedDo = ls_sql, TableUpdated = "OCRD", LineCode = "1112", PaymentCode = item.CardCode, PaymentType = "", Balance = Convert.ToDecimal(oRecordsetLog.Fields.Item("Balance").Value) });
                                            oRecordsetLog.MoveNext();
                                        }

                                        //Run Update 07
                                        oRecordsetVal.DoQuery(ls_sql);
                                        //-----------------------------------------------------------------------------------------------------------------------------------------------

                                    }
                                    else
                                    {
                                        //Change 06-------------------------------------------------------------------------------------------------------------------------------------------
                                        ls_sql = string.Format(Globals.DbQuery.GetString("change06"), differenceToApplique.ToString().Replace(',', '.'), item.CardCode);
                                        ls_sqlLog = string.Format(Globals.DbQuery.GetString("Log0607"), differenceToApplique.ToString().Replace(',', '.'), item.CardCode);

                                        oRecordsetLog.DoQuery(ls_sqlLog);
                                        while (!oRecordsetLog.EoF)
                                        {


                                            Utils.LogChanges(new clsLogChange { UpdatedDo = ls_sql, TableUpdated = "OCRD", LineCode = "1131", PaymentCode = item.CardCode, PaymentType = "", Balance = Convert.ToDecimal(oRecordsetLog.Fields.Item("Balance").Value) });
                                            oRecordsetLog.MoveNext();
                                        }

                                        //Run Update 06
                                        oRecordsetVal.DoQuery(ls_sql);
                                        //-----------------------------------------------------------------------------------------------------------------------------------------------
                                    }

                                }
                            }
                        }

                        break;
                }
            }

        }

        private static void Cancel(ref String formUID, ref SAPbouiCOM.ItemEvent itemEvent, out bool bubbleEvent)
        {
            bubbleEvent = true;

            if (documentCreated != 0)
            {
                SAPbouiCOM.Form form;
                SAPbobsCOM.Documents doc;

                if (typeDocumentCreated == "NC")
                {
                    doc = (SAPbobsCOM.Documents)Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oCreditNotes);

                }
                else
                {
                    doc = (SAPbobsCOM.Documents)Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInvoices);
                }

                doc.GetByKey(documentCreated);
                SAPbobsCOM.Documents docCancel = doc.CreateCancellationDocument();
                docCancel.Add();

                SAPbobsCOM.Recordset oRecordsetVal = null;
                oRecordsetVal = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;

            }
        }

        public static void MenuCancel(bool beforeAction)
        {
            SAPbobsCOM.Recordset oRecordsetVal = null;
            SAPbobsCOM.Recordset oRecordsetValUpdate = null;
            SAPbobsCOM.Recordset oRecordsetIni = null;

            oRecordsetVal = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
            oRecordsetValUpdate = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;
            oRecordsetIni = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;

            SAPbobsCOM.Recordset oRecordsetLog = null;
            oRecordsetLog = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;

            Form form = Globals.SBOApplication.Forms.Item(formID);
            EditText txtDocNum = (EditText)form.Items.Item("3").Specific;
            var table = string.Empty;
            var table16 = string.Empty;
            var ls_sqlIni = string.Empty;

            EditText cardCode;
            cardCode = (EditText)form.Items.Item("5").Specific;

            if (beforeAction)
            {
                var ls_sql = string.Format(Globals.DbQuery.GetString("MenuCancel01"), txtDocNum.Value);
                oRecordsetVal.DoQuery(ls_sql);

                while (!oRecordsetVal.EoF)
                {
                    int docEntry = Convert.ToInt32(oRecordsetVal.Fields.Item("DocEntry").Value.ToString());
                    int type = Convert.ToInt32(oRecordsetVal.Fields.Item("InvType").Value.ToString());
                    decimal rateApplique = Convert.ToDecimal(oRecordsetVal.Fields.Item("DocRate").Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator));
                    decimal montUSD = Convert.ToDecimal(oRecordsetVal.Fields.Item("AppliedFC").Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator));
                    string symbolRate = string.Empty;

                    if (type != 24)
                    {


                        switch (type)
                        {
                            case 13:
                                table = "OINV";
                                table16 = "INV6";

                                ls_sqlIni = string.Format(Globals.DbQuery.GetString("MenuCancel02"), table, docEntry);
                                oRecordsetIni.DoQuery(ls_sqlIni);

                                while (!oRecordsetIni.EoF)
                                {
                                    symbolRate = oRecordsetIni.Fields.Item("DocCur").Value.ToString();
                                    oRecordsetIni.MoveNext();
                                }

                                break;
                            case 14:
                                table = "ORIN";
                                table16 = "RIN6";

                                ls_sqlIni = string.Format(Globals.DbQuery.GetString("MenuCancel02"), table, docEntry);
                                oRecordsetIni.DoQuery(ls_sqlIni);

                                while (!oRecordsetIni.EoF)
                                {
                                    symbolRate = oRecordsetIni.Fields.Item("DocCur").Value.ToString();
                                    oRecordsetIni.MoveNext();
                                }

                                break;
                        }

                        if (symbolRate == "USD" || symbolRate == "EUR" || symbolRate == "USX")
                        {
                            //ls_sql = string.Format(Globals.DbQuery.GetString("change05"), (rateApplique * montUSD).ToString().Replace(',', '.'), docEntry, type.ToString());
                            //oRecordsetValUpdate.DoQuery(ls_sql);

                            //Change 05-------------------------------------------------------------------------------------------------------------------------------------------
                            ls_sql = string.Format(Globals.DbQuery.GetString("change05"), (rateApplique * montUSD).ToString().Replace(',', '.'), docEntry, type.ToString());
                            var ls_sqlLog = string.Format(Globals.DbQuery.GetString("Log05"), (rateApplique * montUSD).ToString().Replace(',', '.'), docEntry, type.ToString());

                            oRecordsetLog.DoQuery(ls_sqlLog);
                            while (!oRecordsetLog.EoF)
                            {


                                Utils.LogChanges(new clsLogChange { Cancel = "Y", UpdatedDo = ls_sql, TableUpdated = "JDT1", LineCode = "1262", PaymentCode = docEntry.ToString(), PaymentType = type.ToString(), Debit = Convert.ToDecimal(oRecordsetLog.Fields.Item("Debit").Value) });
                                oRecordsetLog.MoveNext();
                            }

                            //Run Update 05
                            oRecordsetVal.DoQuery(ls_sql);
                            //-----------------------------------------------------------------------------------------------------------------------------------------------

                            SAPbobsCOM.Recordset oRecordset = null;
                            oRecordset = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;



                        }

                    }

                    oRecordsetVal.MoveNext();
                }
            }
            else
            {
                var ls_sql = string.Format(Globals.DbQuery.GetString("MenuCancel01"), txtDocNum.Value);
                oRecordsetVal.DoQuery(ls_sql);
                decimal rateApplique = 0;
                decimal montUSD = 0;

                while (!oRecordsetVal.EoF)
                {
                    int docEntry = Convert.ToInt32(oRecordsetVal.Fields.Item("DocEntry").Value.ToString());
                    int type = Convert.ToInt32(oRecordsetVal.Fields.Item("InvType").Value.ToString());
                    string symbolRate = string.Empty;

                    if (type != 24)
                    {
                        switch (type)
                        {
                            case 13:
                                table = "OINV";
                                table16 = "INV6";

                                ls_sqlIni = string.Format(Globals.DbQuery.GetString("MenuCancel02"), table, docEntry);
                                oRecordsetIni.DoQuery(ls_sqlIni);

                                while (!oRecordsetIni.EoF)
                                {
                                    rateApplique = Convert.ToDecimal(oRecordsetIni.Fields.Item("DocRate").Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator));
                                    montUSD = Convert.ToDecimal(oRecordsetIni.Fields.Item("DocTotalFC").Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator));
                                    symbolRate = oRecordsetIni.Fields.Item("DocCur").Value.ToString();
                                    oRecordsetIni.MoveNext();
                                }

                                break;
                            case 14:
                                table = "ORIN";
                                table16 = "RIN6";

                                ls_sqlIni = string.Format(Globals.DbQuery.GetString("MenuCancel02"), table, docEntry);
                                oRecordsetIni.DoQuery(ls_sqlIni);

                                while (!oRecordsetIni.EoF)
                                {
                                    rateApplique = Convert.ToDecimal(oRecordsetIni.Fields.Item("DocRate").Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator));
                                    montUSD = Convert.ToDecimal(oRecordsetIni.Fields.Item("DocTotalFC").Value.ToString().Replace(',', Globals.separator).Replace('.', Globals.separator));
                                    symbolRate = oRecordsetIni.Fields.Item("DocCur").Value.ToString();
                                    oRecordsetIni.MoveNext();
                                }

                                break;
                        }

                        if (symbolRate == "USD" || symbolRate == "EUR" || symbolRate == "USX")
                        {
                            //ls_sql = string.Format(Globals.DbQuery.GetString("change05"), (rateApplique * montUSD).ToString().Replace(',', '.'), docEntry, type.ToString());
                            //oRecordsetValUpdate.DoQuery(ls_sql);

                            //Change 05-------------------------------------------------------------------------------------------------------------------------------------------
                            ls_sql = string.Format(Globals.DbQuery.GetString("change05"), (rateApplique * montUSD).ToString().Replace(',', '.'), docEntry, type.ToString());
                            var ls_sqlLog = string.Format(Globals.DbQuery.GetString("Log05"), (rateApplique * montUSD).ToString().Replace(',', '.'), docEntry, type.ToString());

                            oRecordsetLog.DoQuery(ls_sqlLog);
                            while (!oRecordsetLog.EoF)
                            {


                                Utils.LogChanges(new clsLogChange { Cancel = "Y", UpdatedDo = ls_sql, TableUpdated = "JDT1", LineCode = "1345", PaymentCode = docEntry.ToString(), PaymentType = type.ToString(), Debit = Convert.ToDecimal(oRecordsetLog.Fields.Item("Debit").Value) });
                                oRecordsetLog.MoveNext();
                            }

                            //Run Update 05
                            oRecordsetVal.DoQuery(ls_sql);
                            //-----------------------------------------------------------------------------------------------------------------------------------------------

                        }
                    }

                    oRecordsetVal.MoveNext();

                }
            }
        }


        private static clsLogChange getLogchange(string table16, ClsOrdersToProcess order)
        {
            Form form;
            form = Globals.SBOApplication.Forms.Item(formID);
            var txtDocNum = (EditText)form.Items.Item("3").Specific;
            var clsLogChangeReturn = new clsLogChange();
            SAPbobsCOM.Recordset oRecordset = null;
            oRecordset = Globals.SBOCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset) as SAPbobsCOM.Recordset;

            clsLogChangeReturn.IniRate = order.InitRate;

            string sql = string.Format(Globals.DbQuery.GetString("info02"), table16, order.DocEntry);

            oRecordset.DoQuery(sql);

            while (!oRecordset.EoF)
            {
                clsLogChangeReturn.InsTotal = Convert.ToDecimal(oRecordset.Fields.Item("InsTotal").Value);
                oRecordset.MoveNext();
            }

            sql = string.Format(Globals.DbQuery.GetString("info03"), order.DocEntry, order.Type);

            oRecordset.DoQuery(sql);
            while (!oRecordset.EoF)
            {
                clsLogChangeReturn.InsTotal = Convert.ToDecimal(oRecordset.Fields.Item("BalDueDeb").Value);
                oRecordset.MoveNext();
            }

            sql = string.Format(Globals.DbQuery.GetString("info04"), order.DocEntry, order.Type);

            oRecordset.DoQuery(sql);
            while (!oRecordset.EoF)
            {
                clsLogChangeReturn.ReconSum = Convert.ToDecimal(oRecordset.Fields.Item("ReconSum").Value);
                oRecordset.MoveNext();
            }

            sql = string.Format(Globals.DbQuery.GetString("info05"), order.DocEntry, order.Type);

            oRecordset.DoQuery(sql);
            while (!oRecordset.EoF)
            {
                clsLogChangeReturn.Debit = Convert.ToDecimal(oRecordset.Fields.Item("Debit").Value);
                oRecordset.MoveNext();
            }

            sql = string.Format(Globals.DbQuery.GetString("info06"), order.CardCode);

            oRecordset.DoQuery(sql);
            while (!oRecordset.EoF)
            {
                clsLogChangeReturn.Balance = Convert.ToDecimal(oRecordset.Fields.Item("Balance").Value);
                oRecordset.MoveNext();
            }


            return clsLogChangeReturn;
        }

        internal class ClsOrdersToProcess
        {
            public int OrderId { get; set; }
            public int DocEntry { get; set; }
            public string Type { get; set; }
            public decimal InitRate { get; set; }
            public decimal InitMont { get; set; }
            public decimal MontUSD { get; set; }
            public string CardCode { get; set; }
        }
    }
}
